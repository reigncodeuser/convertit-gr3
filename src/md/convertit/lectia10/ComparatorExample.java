package md.convertit.lectia10;

/**
 * Created by Utilizator on 21.11.2016.
 */
public class ComparatorExample {
    public static void main(String[] args) {
        int x = 20;
        int y = 5;
        int z = 20;

        char c1 = 'a';
        char c2 = 'A';
        char c3 = 66; //B
        char c4 = 'a';
        //afisam la consola rezultatul expresie:
        //valoarea lui  x este egala cu valoare lui y
        //valoarea lui  x este egala cu valoare lui z
        //valoarea lui  x este mai mare ca valoare lui z

        //valoarea lui c1 este egala cu valoarea lui c2
        //valoarea lui c2 este mai mare ca valooare lui c2
        //valoarea lui c1 este egala cu valoarea lui c4

        String msg = "mama este acasa";
        //afisam caracter de pe index 0 din msg
        //afisam caracter de pe index 1 din msg
        //valoarea lui c1 este egala cu valoarea char-ului de pe index 0 din msg ?
        //valoarea lui c1 este egala cu valoarea char-ului de pe index 1 din msg ?

        //cu if else realizam urmatorul program
        //daca c1 este egal cu char de pe index 0 din msg afisam mesaj:
        // c1 este egal cu primul char din msg,
        //altfel, afisam mesaj:
        //Ne pare rau, caracterele nu sunt egale
    }

}
