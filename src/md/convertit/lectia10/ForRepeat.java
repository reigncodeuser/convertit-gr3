package md.convertit.lectia10;

/**
 * Created by Utilizator on 21.11.2016.
 */
public class ForRepeat {
    public static void main(String[] args) {
        String txt = "Eu ma duc la scoala";
        //afisam char de pe index 0
        System.out.println(txt.charAt(0));
        //afisam char de pe index 1
        System.out.println(txt.charAt(1));
        //afisam char de pe index 2
        System.out.println(txt.charAt(2));
        //afisam lungimea textului
        int lungime = txt.length();
        System.out.println("lungime text: " + lungime);
        //afisam char de pe ultimul index
        System.out.println(txt.charAt(lungime - 1));
        //facem un for care sa ne arate valoare lui i de la 0 la lungimea textului
        for (int i = 0; i < txt.length(); i++) {
            // System.out.println(i);//charAt(i)
            System.out.println("Pe index " + i +
                    " al string-ului txt avem char: " + txt.charAt(i));
        }
    }
}
