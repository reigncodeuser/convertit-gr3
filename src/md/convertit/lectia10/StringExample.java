package md.convertit.lectia10;

/**
 * Created by Utilizator on 21.11.2016.
 */
public class StringExample {
    public static void main(String[] args) {
        String message = "abecedar";
        //afisam caracterul de pe index 2 al variabile message
        char myChar = message.charAt(2);
        //System.out.println(message.charAt(2));
        System.out.println(myChar);

        String subs = message.substring(1, 4);
        System.out.println(subs);

        String s = "mama";
        String s2 = s.replace('m', 't');
        System.out.println(s2);
        //verficam daca variabila message contine substring-ul "ceda"
        boolean hasCeda = message.contains("ceda");
        System.out.println("contine ceda: ? " + hasCeda);
        //transformam abecedar cu majuscule
        System.out.println("dupa majuscule: " + message.toUpperCase());
    }
}
