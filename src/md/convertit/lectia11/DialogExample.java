package md.convertit.lectia11;

import javax.swing.*;

/**
 * Created by Utilizator on 23.11.2016.
 */
public class DialogExample {
    public static void main(String[] args) {
        //stocam string-ul returnat de showInputDialog in variabila myName
        String myName = JOptionPane.showInputDialog("Cum va numiti?");
        //afisam mesajul cu ajutorul metodei : showMessageDialog()
        JOptionPane.showMessageDialog(null, "Buna ziua " + myName);

        //rugam utiliztorul sa introduca numarul cate repetari doreste
        String repetari = JOptionPane.showInputDialog("Cate repetari");
        // Integer.parseInt(String s) //converteste string in int
        // Double.parseDoule(String s) //converteste string in int
        //sa afiseze numele la consola
        int repetariInt = Integer.parseInt(repetari);//convertesc String in int
        for (int i = 0; i < repetariInt; i++) {
            System.out.println("Repeat " + i + " nume: " + myName);
        }
    }
}
