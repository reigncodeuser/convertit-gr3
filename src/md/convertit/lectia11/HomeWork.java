package lectia11;

/**
 * Created by Dennis on 24.11.2016.
 */
public class HomeWork {

    /**
     *
     * Declaratii de metode:
     *
     * 1.  Declaram o  metoda care primeste 2 parametri , unul String nume, al 2lea int virsta iar metoda sa returneze
     *   un mesaj dupa formatul  : Salut {nume}, tu ai {varsta} ani.
     *
     *   2. Declaram o metoda, care va calcula patratul unui numar , deci trebuie sa accept un parametru de tip int
     *   si sa returneze patratului numarului respectiv
     *
     *   3. declaram o metoda, care va accepta un parametru de tip string si va returna lugimea stringului respectiv
     *
     *   4. declaram o metoda care va accepta un parametru de tip string si va afisa la consola mesajul
     *      The text you entered is {yourString}
     *
     *      In metoda main , apelam fiecare metoda de mai sus.
     *
     */

}
