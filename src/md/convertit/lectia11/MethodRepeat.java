package md.convertit.lectia11;

import javax.swing.*;

/**
 * Created by Utilizator on 25.11.2016.
 */
public class MethodRepeat {

    //declaram o metoda cu tipul de returnare String, numele
    // getMyName cu 0 parametri.
    //Treaba metodei este sa afiseze un dialog (JoptionPane)
    // care sa intrebe
    //utilizatorul ce nume are si sa il returneze.

    public static String getMyName() {
        return JOptionPane.showInputDialog("Your name dude ?");
    }

    //declaram metoda main, in care apelam metoda getMyName
    public static void main(String[] args) {
        String userName = getMyName();
        System.out.printf("Your name is %s", userName);
    }
}
