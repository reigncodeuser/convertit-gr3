package md.convertit.lectia11;

/**
 * Created by Utilizator on 23.11.2016.
 */
public class MethodReturnExample {

    //declaram o metoda cu tipul de returnare int, numele getDifference
    //care accepta 2 parametri de tip int ;
    //In corpul metodei calcula diferenta dintr prima si a 2a variabila
    //si returnam rezultatul
    public static int getDifference(int val1, int val2) {
        int rezultat = val1 - val2;
        return rezultat;
    }

    //declaram medoda maine in care apelam metoda getDifference
    public static void main(String[] args) {
        //stocam intr-o variabila valoare returnata de metoda main
        int rezultReturnat = getDifference(10, 7); //3
        System.out.println("Rezultat este: " + rezultReturnat);
    }

}
