package md.convertit.lectia11;

/**
 * Created by Utilizator on 23.11.2016.
 */
public class SimpleMethodExample {

    public static void sayHello() {
        System.out.println("Hello, suntem in interiorul metodei sayHello");
        return;//optional pentru tip void
    }

    public static void sayHello(String name) {
        System.out.println("Hello " + name +
                ", suntem in interiorul metodei sayHello care are un parametru de tip String");
    }

    public static void main(String[] args) {
        System.out.println("Suntem la inceputul metode main");
        //apelam metoda statica sayHello()
        sayHello();
        sayHello("Denis");
        System.out.println("Suntem la sfirsitul metodei main");
    }

}
