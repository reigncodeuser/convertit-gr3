package md.convertit.lectia12;

/**
 * Created by Utilizator on 25.11.2016.
 */
public class Dulap {
    String culoare;
    double latime;
    double lungime;

    public double calculeazaAreaFund() {
        return latime * lungime;
    }

    public static void main(String[] args) {
        //declaram o variabila de tip Dulap
        Dulap d1;
        //initializam variabila d1
        d1 = new Dulap();
        //afisez la consola culoarea dulapului
        System.out.println(d1.culoare);
        //atribui cimpului culoare din obiectul d1 valoarea negru
        System.out.println("Schimbam culoare");
        d1.culoare = "negru";
        System.out.println(d1.culoare);

        //declar si initilizez o variabila de tip Dulap
        Dulap d2 = new Dulap();
        //setam culoar lui d2
        d2.culoare = "rosu";
        System.out.println("d2 are culoare: " + d2.culoare);
        //setez dulapului d1 latimea 100
        //setez dulapului d2 latimea 300
        //afisez la consola latimea lui d1
        //afisez la consola latimea lui d2

        //afizes daca latimea lui d2 este mai mare ca latimea lui d1
    }
}
