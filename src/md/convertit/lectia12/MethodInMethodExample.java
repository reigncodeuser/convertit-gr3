package md.convertit.lectia12;

import javax.swing.*;

/**
 * Created by Utilizator on 25.11.2016.
 */
public class MethodInMethodExample {

    public static String getName() {
        String name = JOptionPane
                .showInputDialog(null, "Message: Your name dear custumer",
                        "Title: Air Moldova", JOptionPane.QUESTION_MESSAGE);
        return name;
    }

    public static String selectCity() {
        String city = JOptionPane.showInputDialog("Where you want to fly?");
        return city;
    }

    public static void printTicket() {
        String custumerName = getName();
        String cityName = selectCity();
        String template = String.format("Fly to %s \n Customer name: %s", cityName, custumerName);
        JOptionPane.showMessageDialog(null, template, "Ticket details", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void main(String[] args) {
        printTicket();
    }
}
