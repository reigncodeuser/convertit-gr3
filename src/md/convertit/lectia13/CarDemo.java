package md.convertit.lectia13;

/**
 * Created by Utilizator on 28.11.2016.
 */
public class CarDemo {
    public static void main(String[] args) {
        //cream un obiect de tip Car7
        Car c1 = new Car();
        Car c2 = new Car();
        //afisam la consola culoarea obiectului c1
        System.out.println("c1 color: " + c1.color);
        //afisam la consola culoarea obiectului c2
        System.out.println("c2 color: " + c2.color);

        //afisam la consola an fabricare a obiectului c1
        System.out.println("c1 an fabricare: " + c1.anFabricatie);
        //afisam la consola an fabricare a obiectului c2
        System.out.println("c2 an fabricare: " + c2.anFabricatie);

        //atribuim culoarea rosu pentru c1
        c1.color = "rosu";
        //afisam culoarea pentru c1
        System.out.println("c1 culoare dupa atribuire: " + c1.color);
        //atribuim culoare verde pentru c2
        c2.color = "verde";
        System.out.println("c2 culoarea dupa atribuire: " + c2.color);

        //Declaram un obiect de tip Car, numele obiectului: c3
        Car c3;
        //atribui lui c3 referinta lui c2
        c3 = c2;
        System.out.println("c3 culoarea: " + c3.color);
    }
}
