package md.convertit.lectia13;

import java.util.Date;

/**
 * Created by Utilizator on 28.11.2016.
 */
public class MyProgram {
    public static void main(String[] args) {
        Elev e1 = new Elev();
        Elev e2 = new Elev();
        Elev e3 = new Elev();

        Banca b1 = new Banca();
        Banca b2 = new Banca();
        Banca b3 = new Banca();

        e1.nume = "Gicu";
        e1.note = new int[]{10, 9, 7, 9};

        b1.nume = "MAIB";
        b1.adresa = "str. Capcaunilor 3";
        b1.numarClienti = 4300;

        System.out.println(new Date(948758400000L));
    }
}
