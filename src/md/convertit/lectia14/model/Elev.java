package md.convertit.lectia14.model;

/**
 * Created by Utilizator on 30.11.2016.
 */
public class Elev {
    private String name;
    private int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
