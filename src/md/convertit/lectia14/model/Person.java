package md.convertit.lectia14.model;

/**
 * Created by Utilizator on 30.11.2016.
 */
public class Person {
    String name; //default access modifier
    private double baniInPortofel;
    public String porecla;
//setam valori cimpului privat :baniInPortofel
    public void adaugaBani(double sumaBani) {
        if (sumaBani > 0) {
            baniInPortofel = sumaBani;
        }
    }
 //citim valoarea variabilei: baniInPortofel
    public double citiBani() {
        return baniInPortofel;
    }
}
