package md.convertit.lectia14.model;

/**
 * Created by Utilizator on 30.11.2016.
 */
public class PersonTest {
    public static void main(String[] args) {
        Person p1 = new Person();
        p1.name = "Miron Costin";
        System.out.println("Cimpul name e visibil deoarece ne aflam in acelasi" +
                "pachet, si are valoarea: " + p1.name);
        //incerca sa setam valoare pentru bani in portofel al obiect p1
        //p1.baniInPortofel=500;//euro
        //baniInPortofel este un cimp privat si nu este vizibil nici din acelasi
        // pachet
    }
}
