package md.convertit.lectia14.programs;

import md.convertit.lectia14.model.Person;

/**
 * Created by Utilizator on 30.11.2016.
 */
public class AccesModifierTest {

    public static void main(String[] args) {
        //cream un obiect de tip Person
        Person p1 = new Person();//fiind o clasa publica o pot folosi in acesta pachet
        //afisam la consola numele persoanei (campul nume al obiectului p1)
        // System.out.println("cimp nume din obiect p1 este: " + p1.name);
        //nu putem accesa cimpul name deoarece nu este visibil in acest pachet
        System.out.println("cimp porecla din obiect p1 este: " + p1.porecla);
        //setam nume pentru p1
        p1.porecla = "The Boss";
        System.out.println("cimp porecla din obiect p1 este: " + p1.porecla);

        //adaugam bani in portofel pentru p1.
        p1.adaugaBani(300D);
        double baniInPortofel = p1.citiBani();
        System.out.println(p1.porecla + " are " + baniInPortofel + " euro in portofel");

        //bug: nu pot accesa numele lui p1 , si nu pot sa il setez
        //rezolvam bug-ul


    }
}
