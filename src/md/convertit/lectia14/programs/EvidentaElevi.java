package md.convertit.lectia14.programs;

import md.convertit.lectia14.model.Elev;

import javax.swing.*;

/**
 * Created by Utilizator on 30.11.2016.
 */
public class EvidentaElevi {
    public static void main(String[] args) {
        /**
         * Scop: sa cream un program care inregistreaza datele despre un elev,
         * dupa ce datele au fost introduse folosind un dialog, afisam la
         * consola detaliile despre elev
         */
        //declar varibilele temporare pentru a stoca
        // numele sryrtydffgdftgi virsta elevului
        String name;
        int age;
        //initilizez variabilele temporare
        name = JOptionPane.showInputDialog("Introducem nume elev");
        String ageString = JOptionPane.showInputDialog("Introducem virsta elev");
        age = Integer.parseInt(ageString);

        //crea un obiect de tip Elev
        Elev e1 = new Elev();
        //setez nume
        e1.setName(name);
        //setez virsta
        e1.setAge(age);
        System.out.println("Nume elev este : " + e1.getName());
    }
}
