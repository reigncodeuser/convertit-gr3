package md.convertit.lectia15;

/**
 * Created by dench on 03.12.2016.
 */
public class HomeWork {

    /**
     *Realizati urmatoarea clasa:
     *
     *  clasa Animal care are urmatoarele campuri:
     *      nume;
     *      culoare
     *
     *      folosind incapsulearea, adica toate cimpurile sa fie private, getters and setters
     *
     *      Apoi cream 3 obiect de tip animal, si afisam la consola pe fiecare din ele
     *      setam nume si culoare pentur toate 3 si afisam iar  la consola.
     *
     *
     *      Clasa Client care are urmatoarele cimpuri:
     *          Nume, Prenume, virsta, sex.
     *
     *          la fel folosim incapsularea. si constructori , 0 parametri, constructor cu paramtrii nume si virsta,
     *          si un constructor cu toti 4 parametri.
     *
     *      Clasa BiletAvion cu urmatoarele cimpuri:
     *        orasSursa,
     *        orasDestinatie,
     *        pret,
     *        Client (referinta la un obiect de tip client)
     *
     *        Cream un obiect te tip client folosind constructorul cu toti parametrii.
     *
     *        Cream un obiect de tip BiletAvion folosind constructorul cu 0 parametri, iar apoi setam pe rand urmataorele valori:
     *         orasSursa: Chisinau
     *         orasDestinatie: Berlin
     *         pret : 150,
     *         client : setam referinta obiectului client care lam declarat mai sus.
     *
     *         //afisam la consola biletul de avion
     *
     *         OPTIONAL:
     *         Cream inca un obiect de tip BiletAvion insa o sa ne roage sa introducem cu JoptionPane cimpurile de mai sus.
     *         Afisam la consola informatiile introduse de utilizator pentru biletul de avion.
     */

}
