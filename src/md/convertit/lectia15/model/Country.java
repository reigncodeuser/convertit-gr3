package md.convertit.lectia15.model;

/**
 * Created by Utilizator on 02.12.2016.
 */
public class Country {
    // name
    // capital
    // population
    // isEUMember - boolean
    private String name;
    private String capital;
    private long population;
    private boolean isEUMember;

    public Country(){
        this.name="NO NAME SET";
    }

    public Country(String name,String capital){
        this.name = name;
        this.capital=capital;
    }

    public Country(String name, String capital, long population, boolean isEUMember) {
        this.name = name;
        this.capital = capital;
        this.population = population;
        this.isEUMember = isEUMember;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public boolean isEUMember() {
        return isEUMember;
    }

    public void setEUMember(boolean EUMember) {
        isEUMember = EUMember;
    }

    @Override
    public String toString() {
        return "Country ----> " +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", population=" + population +
                ", isEUMember=" + isEUMember;
    }
}
