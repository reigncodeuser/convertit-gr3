package md.convertit.lectia15.model;

/**
 * Created by Utilizator on 02.12.2016.
 */
public class Engine {
    // 2 campuri: capacity , fuelType
    private int capacity;
    private String fuelType;
    //constructor: 0 params si cu toti parametrii

    public Engine() {
    }

    public Engine(int capacity, String fuelType) {
        this.capacity = capacity;
        this.fuelType = fuelType;
    }
    //getters and setters

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "capacity=" + capacity +
                ", fuelType='" + fuelType + '\'' +
                '}';
    }
}
