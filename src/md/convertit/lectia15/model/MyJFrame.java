package md.convertit.lectia15.model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Utilizator on 02.12.2016.
 */
public class MyJFrame extends JFrame {

    public MyJFrame(String title) throws HeadlessException {
        super(title);
    }

    public static void main(String[] args) {
        MyJFrame myJFrame = new MyJFrame("My JFrame");
        myJFrame.setSize(500, 300);
        myJFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        myJFrame.setLocationRelativeTo(null);


        JPanel jPanel = new JPanel(new GridLayout(3,3));

        JButton button1 = new JButton("1");
        JButton button2 = new JButton("2");
        JButton button3 = new JButton("3");
        JButton button4 = new JButton("4");
        JButton button5 = new JButton("5");
        JButton button6 = new JButton("6");
        JButton button7 = new JButton("7");
        JButton button8 = new JButton("8");
        JButton button9 = new JButton("9");
        JLabel jLabel = new JLabel("Aici va aparea informatia");

        myJFrame.getContentPane().add(jPanel);
        jPanel.add(button1);
        jPanel.add(button2);
        jPanel.add(button3);
        jPanel.add(button4);
        jPanel.add(button5);
        jPanel.add(button6);
        jPanel.add(button7);
        jPanel.add(button8);
        jPanel.add(button9);
//        jPanel.add(jLabel);
//
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                jLabel.setText(JOptionPane.showInputDialog("Cum te cheama?"));
//            }
//        });

        myJFrame.setVisible(true);
    }
}
