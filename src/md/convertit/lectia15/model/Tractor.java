package md.convertit.lectia15.model;

/**
 * Created by Utilizator on 02.12.2016.
 */
public class Tractor {
    private String model;
    private Engine engine;
    //constructor cu 0 params si constructor cu toti parametrii

    public Tractor() {
    }

    public Tractor(String model, Engine engine) {
        this.model = model;
        this.engine = engine;
    }
//getters and setters

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    //toString (optional)
    @Override
    public String toString() {
        return "Tractor{" +
                "model='" + model + '\'' +
                ", engine=" + engine +
                '}';
    }
}
