package md.convertit.lectia15.programs;

import md.convertit.lectia15.model.Country;

/**
 * Created by Utilizator on 02.12.2016.
 */
public class CountryList {
    public static void main(String[] args) {
        //declaram si initializam un obiect de tip Coutry
        Country c1 =new Country();
        System.out.println("c1 name: " + c1.getName());
        System.out.println("c1 capital: " + c1.getCapital());
        System.out.println("c1 population: "+ c1.getPopulation());
        System.out.println("c1 is EU member : "+ c1.isEUMember());
        //setam nume tara, capitala , populatie si isEUMember
        c1.setName("Moldova");
        c1.setCapital("Chișinău");
        c1.setPopulation(3000000L);
        System.out.println("c1 name: " + c1.getName());
        System.out.println("c1 capital: " + c1.getCapital());
        System.out.println("c1 population: "+ c1.getPopulation());
        System.out.println("c1 is EU member : "+ c1.isEUMember());

        Country c2 = new Country("India","New Delhi");
        System.out.println("c2 name: " + c2.getName());
        System.out.println("c2 capital: " + c2.getCapital());
        System.out.println("c2 population: "+ c2.getPopulation());
        System.out.println("c2 is EU member : "+ c2.isEUMember());
        c2.setPopulation(160000000L);
        System.out.println("c2 population: " + c2.getPopulation());

        //Franta -obiect - folosim constructorul cu toti paramtrii
        Country c3 = new Country("France", "Paris", 66000000L, true);
        System.out.println(c3);
    }
}
