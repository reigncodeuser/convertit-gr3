package md.convertit.lectia15.programs;

import md.convertit.lectia15.model.Engine;
import md.convertit.lectia15.model.Tractor;

/**
 * Created by Utilizator on 02.12.2016.
 */
public class TractorTest {
    public static void main(String[] args) {

        //cream un obiect de tip Motor (constructor full)
        Engine engine  = new Engine(3000,"DIESEL");
        //cream un obiect de tip Tractor (constructor full)
        Tractor tractor = new Tractor("MTZ-80",engine);
        //cream un alt obiect de tip Tractor //constructor  0
        Tractor t2 = new Tractor();
        t2.setModel("TT-90");
        //setam obiectul motor la noul obiect Tractor
        t2.setEngine(engine);
        System.out.println(t2);

        //declar o variabila de tip Engine
        Engine e=tractor.getEngine();
        System.out.println(e);

        //Declaram o variabila de tip Remorca
        //facem referinta la remorca tractorului (care il avem declarat deja)
        //afisam la consola remorca
    }
}
