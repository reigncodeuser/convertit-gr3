package md.convertit.lectia16.models;

/**
 * Created by Utilizator on 05.12.2016.
 */
public class Animal extends Object{
    private String color;
    private String breed;//rasa, schimbam dupa mostenire

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}
