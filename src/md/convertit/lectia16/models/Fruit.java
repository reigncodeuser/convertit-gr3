package md.convertit.lectia16.models;

/**
 * Created by Utilizator on 05.12.2016.
 */
public class Fruit {
    private String origin;

    public Fruit(String origin) {
        System.out.println("Sunt in constructor din clasa Fruit");
        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
