package md.convertit.lectia16.programs;

import md.convertit.lectia16.models.Animal;
import md.convertit.lectia16.models.Cat;
import md.convertit.lectia16.models.DingoDog;
import md.convertit.lectia16.models.Dog;

/**
 * Created by Utilizator on 05.12.2016.
 */
public class InheritanceTest {
    public static void main(String[] args) {
        //cream un obiect de tip animal
        Animal a1 = new Animal();
        a1.setColor("Black");
        a1.setBreed("Hippo");
        System.out.println("a1 color: " + a1.getColor());
        System.out.println("a1 breed: " + a1.getBreed());
        //cream un obiect de tip Dog
        Dog d1 = new Dog();
        d1.setColor("Brown");
        d1.setBreed("Pekinesse");
        System.out.println("d1 color: " + d1.getColor());
        System.out.println("d1 breed: " + d1.getBreed());

        a1 = d1; // referintele de tip Animal se pot referi la obiectele
        //care mostenesc Animal
        System.out.println("dupa atribuire, a1 color: " + a1.getColor());
        //  d1=a1; invalid
        d1.bark();
        Object o = a1;
        // o.getColor();//invalid

        Cat cat = new Cat();
        cat.meow();
        //cream un obiect de tip Pisica folosind ca referinta clasa Animal
        Animal c1 = new Cat();
        // c1.meow(); //invalid */

        DingoDog dd = new DingoDog();
        dd.bark();
    }
}
