package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 07.12.2016.
 */
public abstract class Animal {
    private String specie;

    public abstract void scoateSunt();

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }
}
