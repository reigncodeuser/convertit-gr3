package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 07.12.2016.
 */
public abstract class Artist {
    private String name;

    public abstract void sing();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
