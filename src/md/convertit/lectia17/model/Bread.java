package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 07.12.2016.
 */
public class Bread extends Product {
    @Override
    public double getRetailPrice() {
        double adaos = getNetPrice()*0.05;
        return getNetPrice()+adaos;
    }
}
