package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 07.12.2016.
 */
public class Concert {
    public static void main(String[] args) {
        Artist artist1 = new Eminem();
        Artist artist2 = new CelineDion();
        Artist artist3 = new AlaPugaciova();

        artist1.sing();
        artist2.sing();
        artist3.sing();
    }
}
