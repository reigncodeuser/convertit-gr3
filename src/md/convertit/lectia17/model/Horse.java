package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 07.12.2016.
 */
public class Horse extends Animal {
    private boolean domestic;

    public boolean isDomestic() {
        return domestic;
    }

    public void setDomestic(boolean domestic) {
        this.domestic = domestic;
    }

    @Override
    public void scoateSunt() {
        System.out.println("Ni-ha-ha");
    }
}
