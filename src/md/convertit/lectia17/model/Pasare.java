package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 07.12.2016.
 */
public class Pasare extends Animal {
    private boolean vanatoare;

    public boolean isVanatoare() {
        return vanatoare;
    }

    public void setVanatoare(boolean vanatoare) {
        this.vanatoare = vanatoare;
    }

    @Override
    public void scoateSunt() {
        System.out.println("Cip cirip!!!");
    }
}
