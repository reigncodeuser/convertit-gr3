package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 07.12.2016.
 */
public abstract class Product {
    private double netPrice;

    public abstract double getRetailPrice();

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }
}
