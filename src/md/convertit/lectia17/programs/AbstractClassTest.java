package md.convertit.lectia17.programs;

import md.convertit.lectia17.model.Animal;
import md.convertit.lectia17.model.Horse;
import md.convertit.lectia17.model.Pasare;

/**
 * Created by Utilizator on 07.12.2016.
 */
public class AbstractClassTest {
    public static void main(String[] args) {
        //   Animal a =new Animal();
        Horse h1 = new Horse();
        //populam fielduri (cimpuri)
        h1.setSpecie("Mustang");
        h1.setDomestic(false);

        Animal a = h1; // horse este un animal ? Da

        Pasare p = new Pasare();
        p.setSpecie("hulub");
        p.setVanatoare(false);

        a = p; //pasare este un animal ? Da

        p.scoateSunt();
        h1.scoateSunt();
        System.out.println("*********");
        Animal a2 = h1;
        a2.scoateSunt();
        a2=p;
        a2.scoateSunt();
    }
}
