package md.convertit.lectia17.programs;

import javax.swing.*;

/**
 * Created by Utilizator on 07.12.2016.
 */
public class StringTest {
    public static void main(String[] args) {
      //  String txt = "Eu plec in vacanta";
        String txt = JOptionPane.showInputDialog("Enter text please");
        //declar variabilele de lucru
        int numarSpatii = 0;
        int numarMajuscule = 0;
        int txtLenth = txt.length();

        for (int i = 0; i < txtLenth; i++) {
            char myChar = txt.charAt(i);
            //System.out.println(myChar);
            boolean isSpace = Character.isSpaceChar(myChar);
            //verificam daca caracterul este spatiu
            if (isSpace) {
                numarSpatii++;
            }

            boolean isUpperCase = Character.isUpperCase(myChar);
            if (isUpperCase) {
                numarMajuscule++;
            }
        }
        System.out.println("Total spatii avem: " + numarSpatii);
        System.out.println("Total majuscule avem: " + numarMajuscule);
    }
}
