package md.convertit.lectia18.impl;

import md.convertit.lectia18.Displayable;

/**
 * Created by Utilizator on 09.12.2016.
 */
public class ConsoleDisplay implements Displayable {

    @Override
    public void displayMessage(String txt) {
        System.out.println("Console Display: " + txt);
    }
}
