package md.convertit.lectia18.impl;

import md.convertit.lectia18.Displayable;

import javax.swing.*;

/**
 * Created by Utilizator on 09.12.2016.
 */
public class ErrorDisplay implements Displayable {
    @Override
    public void displayMessage(String txt) {
        JOptionPane.showMessageDialog(null, txt, "Eroare fatala", JOptionPane.ERROR_MESSAGE);
    }
}
