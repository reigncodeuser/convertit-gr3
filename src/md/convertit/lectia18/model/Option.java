package md.convertit.lectia18.model;

/**
 * Created by Utilizator on 09.12.2016.
 */
public class Option {
    private int option;
    private String description;

    public Option(int option, String description) {
        this.option = option;
        this.description = description;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
