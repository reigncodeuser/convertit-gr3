package md.convertit.lectia18.programs;

import md.convertit.lectia18.Displayable;
import md.convertit.lectia18.impl.ConsoleDisplay;
import md.convertit.lectia18.impl.DialogDisplay;

/**
 * Created by Utilizator on 09.12.2016.
 */
public class IntfExample {
    public static void main(String[] args) {
        Displayable d = new ConsoleDisplay();
        d.displayMessage("Hello");
        d = new DialogDisplay();
        d.displayMessage("Hello");
    }
}
