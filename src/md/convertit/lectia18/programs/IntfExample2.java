package md.convertit.lectia18.programs;

import md.convertit.lectia18.Displayable;
import md.convertit.lectia18.impl.ConsoleDisplay;
import md.convertit.lectia18.impl.DialogDisplay;
import md.convertit.lectia18.impl.ErrorDisplay;

import javax.swing.*;
import java.util.Scanner;

/**
 * Created by Utilizator on 09.12.2016.
 */
public class IntfExample2 {
    public static void main(String[] args) {
        //declaram si initializam o variabila de tip String, txt

        //rugam utilizatorul sa aleaga una din optiunile de
        //afisare a textului:
        /*
        1 - afisare la consola
        2- afisare la dialog
        Daca a ales o alta optiune roaga sa mai incerce odata.
         */
        String txt = "Eu sunt textul mult dorit";
        System.out.println("Selectati optiuni de afisare");
        System.out.println(" 1 - afisare la consola");
        System.out.println(" 2- afisare la dialog");
        Object[] allOptions = new Object[]{0, 1, 2};
        Object strinOption = JOptionPane.showInputDialog(null, "Alege o optiune",
                "Optiuni afisare", JOptionPane.QUESTION_MESSAGE, null, allOptions, allOptions[2]);
        Integer selection = (Integer) strinOption;
        int optiuneSelectata = selection;
        //  int optiuneSelectata =3;
        Displayable displayable;

        if (optiuneSelectata == 1) {
            //afisam la consola
            displayable = new ConsoleDisplay();
        } else if (optiuneSelectata == 2) {
            //afisam in dialog
            displayable = new DialogDisplay();
        } else {
            //notificam de eroare
            // System.out.println("Eroare fatala");
            txt = "Nici o optiune aleasa nu este valida, La revedere";
            displayable = new ErrorDisplay();
        }
        //apelam metoda din intefata Displayable
        displayable.displayMessage(txt);

/*        Object[] choices = { 1, 2};
        String input = (String) JOptionPane.showInputDialog(null,
                "Choose display method...",
                "How to display?",
                JOptionPane.QUESTION_MESSAGE, null,
                choices,
                choices[0]); // Initial choice*/
    }
}
