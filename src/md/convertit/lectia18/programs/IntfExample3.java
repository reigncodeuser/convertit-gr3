package md.convertit.lectia18.programs;

import md.convertit.lectia18.Displayable;
import md.convertit.lectia18.impl.ConsoleDisplay;
import md.convertit.lectia18.impl.DialogDisplay;
import md.convertit.lectia18.impl.ErrorDisplay;
import md.convertit.lectia18.model.Option;

import javax.swing.*;

/**
 * Created by Utilizator on 09.12.2016.
 */
public class IntfExample3 {
    public static void main(String[] args) {
        String txt = "Eu sunt textul mult dorit";
        Option option1 = new Option(0, "Nici o optiune");
        Option option2 = new Option(1, "Afisam in consola");
        Option option3 = new Option(2, "Afisam cu dialog");
        Option[] allOptions = new Option[]{option1, option2, option3};
        Object stringOption = JOptionPane.showInputDialog(null, "Alege o optiune",
                "Optiuni afisare", JOptionPane.QUESTION_MESSAGE, null, allOptions, allOptions[0]);
        Option selectionOption = (Option) stringOption;
        int optiuneSelectata = selectionOption.getOption();
        //  int optiuneSelectata =3;
        Displayable displayable;

        if (optiuneSelectata == 1) {
            //afisam la consola
            displayable = new ConsoleDisplay();
        } else if (optiuneSelectata == 2) {
            //afisam in dialog
            displayable = new DialogDisplay();
        } else {
            //notificam de eroare
            // System.out.println("Eroare fatala");
            txt = "Nici o optiune aleasa nu este valida, La revedere";
            displayable = new ErrorDisplay();
        }
        //apelam metoda din intefata Displayable
        displayable.displayMessage(txt);

/*        Object[] choices = { 1, 2};
        String input = (String) JOptionPane.showInputDialog(null,
                "Choose display method...",
                "How to display?",
                JOptionPane.QUESTION_MESSAGE, null,
                choices,
                choices[0]); // Initial choice*/
    }
}
