package md.convertit.lectia19;

import javax.swing.*;

/**
 * Created by Utilizator on 12.12.2016.
 */
public class ExceptionExample {

    public static void main(String[] args) {
        String luckyNumberAsString =
                JOptionPane.showInputDialog("Your lucky number please!");
        int luckNumber;
        try {
            luckNumber = Integer.parseInt(luckyNumberAsString);
            System.out.println("Your lucky number is : " + luckNumber);
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,"You shoul add a valid number",
                    "Fatal error",JOptionPane.ERROR_MESSAGE);
        }finally {
            //se executa indiferent de statutul executiei
            System.out.println("am iesit din try catch");
        }
    }

}
