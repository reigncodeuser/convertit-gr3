package md.convertit.lectia19;

import javax.swing.*;

/**
 * Created by Utilizator on 12.12.2016.
 */
public class FacebookExample {
    public static void main(String[] args) {
        //rugam utilizator sa introduca numele
        String nume = JOptionPane.showInputDialog("Your name please!");
        //rugam sa introducem email
        String email = JOptionPane.showInputDialog("Your email please");
        //rugam utilizator sa introduca virsta
        int virsta = Integer.parseInt(JOptionPane.showInputDialog("Your age plase"));
        //inregistram in retea sociala
        SocialNetwork sn = new SocialNetwork();
        try {
            sn.registerUser(virsta,nume,email);
            System.out.println("Felicitari, ati fost inregistrat");
        } catch (MyCustomException e) {
            JOptionPane.showMessageDialog(null,e.getMessage(),
                    "Error at: " + e.getDate(),JOptionPane.ERROR_MESSAGE);
        } catch (InvalidEmailException e) {
            JOptionPane.showMessageDialog(null,"Your email is invalid");
        }
    }
}
