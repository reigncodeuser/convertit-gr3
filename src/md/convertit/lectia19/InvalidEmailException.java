package md.convertit.lectia19;

/**
 * Created by Utilizator on 12.12.2016.
 */
public class InvalidEmailException extends Exception {

    public InvalidEmailException(String message) {
        super(message);
    }
}
