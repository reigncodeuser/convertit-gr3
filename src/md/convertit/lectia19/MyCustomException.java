package md.convertit.lectia19;

import java.util.Date;

/**
 * Created by Utilizator on 12.12.2016.
 */
public class MyCustomException extends Exception{
    private Date date;

    public MyCustomException(String message) {
        super(message);
        date = new Date();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
