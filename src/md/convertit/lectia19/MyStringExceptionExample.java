package md.convertit.lectia19;

import javax.swing.*;

/**
 * Created by Utilizator on 12.12.2016.
 */
public class MyStringExceptionExample {
    public static void main(String[] args) {
        String myValue = JOptionPane
                .showInputDialog("Hey, put your number here");

        try {
            int myInt = MyStringUtil.convertStringToInt(myValue);
            System.out.println("Your int is " + myInt);
        }catch (Exception e){
            System.out.println("Your exception: " + e.getMessage());
        }
    }
}
