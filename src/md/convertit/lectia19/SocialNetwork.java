package md.convertit.lectia19;

/**
 * Created by Utilizator on 12.12.2016.
 */
public class SocialNetwork {

    public boolean registerUser(int age, String name, String email)
            throws MyCustomException, InvalidEmailException {
        registerUser(age, name);

        if (!email.contains("@")) {
            throw new InvalidEmailException("Your email is invalid");
        }
    //if no exception return true
        return true;
    }

    public boolean registerUser(int age, String name) throws MyCustomException {
        //validam virsta
        if (age < 12) {
            throw new MyCustomException("You are a kid, try later");
        }
        //validam numele]
        if (name.length() < 3) {
            throw new MyCustomException("Enter a valid name please");
        }

        return true;
    }

}
