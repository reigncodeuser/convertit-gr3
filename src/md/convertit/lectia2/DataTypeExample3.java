package md.convertit.lectia2;

/**
 * Created by Utilizator on 04.11.2016.
 */
public class DataTypeExample3 {

    public static void main(String[] args) {
        //declaram o variabila de tip char, numele lastLetter
        // si o initializam cu valoarea: Z
        //afisam la consola

        //declaram si initializam o variabila de tip long cu valoarea 1000
        //afisam la consola

        //declaram o variabila de tip int, notaElevului
        //afisam la consola valoarea variabilei "notaElevului"

        //initializam notaElevului cu valoarea 10
        //afisam la ecran

        //declaram 5 variabile de tip int cu numele :
        // nota1, nota2, nota3, nota4, nota5 cu valorile : 6,7,6,9,9
        //afisam la consola toate variabile

        // analizam urmatorul statement (expresie) java
        int numarElevi = 25;
        // rapundem la urmatoarele intrebari:
        //1.  care este variabila din expresie ?
        //2. care este tipul variabilei ?
        // 3.ce este  semnul "=" din expresie
        //4. ce este "25" din expresie
        //5. ce este ";"

        //6. care este diferenta intre a declara si a initiliza o variabila ?

        //7. care tipuri de date le stim ?

    }
}
