package md.convertit.lectia2;

/**
 * Created by Utilizator on 02.11.2016.
 */
//am declarat o clasa cu numele DataTypesExample
public class DataTypesExample {

    //declaram metoda main
    public static void main(String[] args) {
        // [tip] numeVariabila ;
        // [tip] =>  byte, short, int, long, float, double, boolean, char
        //declaram o variabila de tip byte
        byte notaByte;
        //initializez variabila notaByte
        //  numeVariabila = [valoareVariabila] ;
        notaByte = 10;
        //afisez la ecran valoarea variabilei notaByte
        System.out.println(notaByte);
        //declar si initializez o variabila de tip int, cu nume nota, valoarea 7
        // [tip] numeVariabila = [valoareVariabila] ;
        int nota = 7;
        //afisam la ecran valoarea variabilei nota
        System.out.println(nota);

        //declar o variabila de tip int cu numele numarLocuri ;
        int numarLocuri;
        //initializez variabila cu valoarea 44;
        numarLocuri = 44;
        //afisez la ecran valoarea variabilei numarLocuri
        System.out.println(numarLocuri);
        // declaram si initializam o variabila de tip double
        //cu numele pretPaine si valoarea 4.5
        double pretPaine = 4.5;
        //afisam la ecran pretul painii
        System.out.println(pretPaine);
    }


}


