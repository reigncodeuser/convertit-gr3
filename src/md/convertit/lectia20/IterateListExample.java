package md.convertit.lectia20;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 14.12.2016.
 */
public class IterateListExample {
    public static void main(String[] args) {
        List<Integer> note = new ArrayList<>();

        List<Integer> initialValues = new ArrayList<>();
        initialValues.add(1);
        initialValues.add(2);
        initialValues.add(3);
        initialValues.add(4);
        initialValues.add(5);

        //adaug toate elementele altei liste
        note.addAll(initialValues);
        note.add(6);
        note.add(7);
        note.add(8);
        note.add(9);
        note.add(10);

        //iteram prin toate elementele folosind index
   /*     for (int i = 0; i < note.size(); i++) {
            System.out.println("pe index " + i + " avem valoare "
                    + note.get(i));
        }*/
        System.out.println("**********");
        for (int nota : note) {
            System.out.println("nota este: " + nota);
        }
        System.out.println("##########");
        note.removeAll(initialValues);
        for (int nota : note) {
            System.out.println("nota este: " + nota);
        }
    }
}
