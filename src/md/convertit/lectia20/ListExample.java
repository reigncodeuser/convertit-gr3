package md.convertit.lectia20;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 14.12.2016.
 */
public class ListExample {
    public static void main(String[] args) {
        List list1 = new ArrayList();
        // medota add() adauga elemente in colectie
        list1.add("Luni");
        list1.add(10);
        // size() - arata marimea listei
        System.out.println("marime list1 : " + list1.size());

        Object index0onList = list1.get(0);
        System.out.println("pe index 0 avem: " + index0onList);
        // remove() sterge obiect din lista
        //stergem de pe index 0
        list1.remove(0);
       //afisam marimea listei
        System.out.println("marime list1: " + list1.size());
        System.out.println("Pe index 0 acum avem: " + list1.get(0));
        System.out.println("Pe index 1 acum avem: " + list1.get(1));
    }
}
