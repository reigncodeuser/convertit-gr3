package md.convertit.lectia20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Utilizator on 14.12.2016.
 */
public class NoNegativeNumbers {
    public static void main(String[] args) {
        Integer[] initValues = new Integer[]{-5, 10, 12, -6, -9, 4, -50};
        List<Integer> allNumbers = new ArrayList<>(Arrays.asList(initValues));
        for (int number : allNumbers) {
            System.out.println("number is " + number);
        }
        System.out.println("Cum stergem toate numerele negative din " +
                "colectia initiala ?");
        //creez o lista noua pentru a pastra numerele negative
        List<Integer> negativeNumbersList = new ArrayList<>();
        //iterez prin toate elementele listei initiale
        for (Integer number : allNumbers) {
            if (number < 0) {
                //daca numar e negativ il adaug in lista: negativeNumbersList
                negativeNumbersList.add(number);
            }
        }//end for
        //stergem din lista initiala toate elementele negative

        allNumbers.removeAll(negativeNumbersList);
        System.out.println("am sters: " + negativeNumbersList);
        System.out.println("au ramas: " + allNumbers);
    }
}
