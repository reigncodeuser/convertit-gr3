package md.convertit.lectia20;

import md.convertit.lectia17.model.Animal;
import md.convertit.lectia17.model.Horse;
import md.convertit.lectia17.model.Pasare;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Utilizator on 14.12.2016.
 */
public class SetExample {
    public static void main(String[] args) {
//        List<Animal> myAnimals = new ArrayList<>();
        Set<Animal> myAnimals = new HashSet<>();
        Animal a1 = new Pasare();
        Animal a2 = new Horse();
        myAnimals.add(a1);
        myAnimals.add(a2);
        //my  adaugam odata pe a2
        myAnimals.add(a2);
        myAnimals.add(new Pasare());
        myAnimals.add(new Horse());
        System.out.println("total animale am: " + myAnimals.size());
    }
}
