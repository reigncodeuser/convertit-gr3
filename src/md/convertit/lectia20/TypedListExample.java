package md.convertit.lectia20;

import md.convertit.lectia17.model.Animal;
import md.convertit.lectia17.model.Horse;
import md.convertit.lectia17.model.Pasare;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 14.12.2016.
 */
public class TypedListExample {
    public static void main(String[] args) {
        //cream un obiect de tip ArrayList folosind ca tip de referinta
        //inderfata List
        List<String> weekDays = new ArrayList<>();
        String firstDay = "monday";
        weekDays.add(firstDay);
        weekDays.add("tuesday");
        weekDays.add(new String("wednesday"));
        System.out.println("total elements now: " + weekDays.size());
        weekDays.add(firstDay);
        System.out.println("total elements now: " + weekDays.size());
        //incercam sa adaugam un int
        //weekDays.add(3); //nu accepta alte elemente decat Stringuri
        //stergem element firstDay
        weekDays.remove(firstDay);
        System.out.println("dupa stergere firstDay, marimea este " + weekDays.size());

        boolean containsTuesday = weekDays.contains("tuesday");
        System.out.println("contains tuesday: " + containsTuesday);
        System.out.println("contains sunday? " + weekDays.contains("sunday"));
        //curatim lista
        weekDays.clear();
        System.out.println("dupa curatire marimea este " + weekDays.size());

        List<Animal> myAnimals = new ArrayList<>();
        Animal a1 = new Pasare();
        Animal a2 = new Horse();
        myAnimals.add(a1);
        myAnimals.add(a2);
        myAnimals.add(new Pasare());
        myAnimals.add(new Horse());
        System.out.println("total animale am: " + myAnimals.size());
    }
}
