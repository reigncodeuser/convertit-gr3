package md.convertit.lectia21;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 16.12.2016.
 */
public class ListStringExercice {
    public static void main(String[] args) {
        String[] cities = new String[]{"Chisinau",
                "Chicago", "Lisabona", "Calarasi", "Cricova",
                "Bucuresti", "Balti", "Brasov", "Leova"};
        //trebuie sa stocam intro lista orasele care se incep cu 'C'
        //si in alta cele care cu 'B' si apoi le
        List<String> cityC = new ArrayList<>();//stocam cele cu C
        List<String> cityB = new ArrayList<>();//stocam cele cu B
        List<String> cityOther = new ArrayList<>();//stocam celelalte
        //alegem obiectele si le sortam
        for (int i = 0; i < cities.length; i++) {
            String currentCity = cities[i];
            if (currentCity.startsWith("C")) {
                cityC.add(currentCity);
            } else if (currentCity.startsWith("B")) {
                cityB.add(currentCity);
            } else {
                cityOther.add(currentCity);
            }
        }
        System.out.println("Cu C avem: " + cityC);
        System.out.println("Cu B avem: " + cityB);
        System.out.println("Altele sunt: " + cityOther);
    }
}
