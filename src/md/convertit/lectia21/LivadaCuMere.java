package md.convertit.lectia21;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Utilizator on 16.12.2016.
 */
public class LivadaCuMere {
    /*
    Cream un program care sa realizeze urmatoarele functionalitati
    1. Sa roage utilizatorul sa introduca de la tastatura numarul de
    mere care vrea sa le genereze
    2. Generam numarul de mere cat a ales utilizatorul.
    Fiecare mar trebuie sa aiba una din culorile predefinite
    (folosim random) pentru a alege un index aleator din tablou de culori ,
    si setam culoarea la mar, sau putem atribui culoare si prin constructor

    3.Modificam programul in asa fel ca sa ne sorteze merele dupa culori,
    si la urma sa ne afiseze cate mere avem de fiecare culoare,
    ex: Total mere de culoare 'rosie' avem 5
              */

    public static void main(String[] args) {
        String[] culori = new String[]{"rosu", "verde", "galben"};
        int numarInitialMere = 20;
        Random rand = new Random();
        //cream lista initiala de mere
        List<Appl> myApples = new ArrayList<>();
        for(int i=0;i<numarInitialMere;i++){
            int indexCuloare = (rand.nextInt(3));
            String culoare = culori[indexCuloare];
            Appl appl = new Appl(culoare);
            //adauga in lista
            myApples.add(appl);
        }
    }
}
class Appl {
    private String color;

    public Appl(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}