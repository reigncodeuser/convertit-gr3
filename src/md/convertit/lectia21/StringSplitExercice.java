package md.convertit.lectia21;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 16.12.2016.
 */
public class StringSplitExercice {
    public static void main(String[] args) {
        //Rugam utilzatorul sa aleaga o litera
        //Apoi il rugam sa scrie o fraza
        //Programul nostru trebue sa arate cuvintele din fraza care contin
        //litera aleasa anterior.
        String selectedString = JOptionPane.showInputDialog("Scrie o litera");
        System.out.println("litera selectat: " + selectedString);
        String fraza = JOptionPane.showInputDialog("Scrie o fraza");
        System.out.println("Fraza scrisa: " + fraza);
        //impart fraza dupa spatiu sa obtin cuvintele
        String[] words = fraza.split(" ");
        System.out.println("Total cuvinte in fraza: " + words.length);
        //aflu doar cuvintele care contin litera aleasa de utilizator
        //si le pun intr-o lista
        List<String> myList = new ArrayList<>();
        for (String word : words) {
            if (word.contains(selectedString)) {
                myList.add(word);
            }
        }
        System.out.println("Litera: " + selectedString + " se contine in "
                + myList);
    }
}
