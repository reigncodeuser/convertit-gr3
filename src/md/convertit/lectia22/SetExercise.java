package md.convertit.lectia22;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Utilizator on 19.12.2016.
 */
public class SetExercise {
    public static void main(String[] args) {
        //declaram si initializam un obiect de tip HashSet
        Set<String> mySet = new HashSet<>();
        mySet.add("luni");
        mySet.add("marti");
        mySet.add(new String("miercuri"));
        System.out.println("my set are marimea: " + mySet.size());
        System.out.println("adaugam 'luni' in set: " + mySet.add("luni"));
        System.out.println("adaugam 'Luni' in set: " + mySet.add("Luni"));
        System.out.println("my set are marimea: " + mySet.size());
        System.out.println("sterg 'luni': " + mySet.remove("luni"));
        System.out.println("my set are marimea: " + mySet.size());
    }
}
