package md.convertit.lectia22.gui;

import md.convertit.lectia22.model.Person;
import md.convertit.lectia22.util.DateUtil;
import md.convertit.lectia22.util.PersonUtil;

import javax.swing.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Utilizator on 19.12.2016.
 */
public class RegistruElectronic {

    private static final String TEMPLATE = "Introduceti datele dupa formatul:\n" +
            "Nume Prenume,dd/MM/YYYY";

    final static String[] OPTIONS = new String[]{
            "Persoana nou",
            "Vezi lista",
            "Iesire program"};

    public static void main(String[] args) throws ParseException {


        JOptionPane.showMessageDialog(null, "Bine ati venit!");
        List<Person> catalogPersoane = new ArrayList<>();
        while (true) {
            //afisam dialog de confirmare
            Object userChoise =
                    JOptionPane.showInputDialog(null, "Ce facem mai departe?",
                            "Atentie", JOptionPane.QUESTION_MESSAGE,
                            null, OPTIONS, OPTIONS[0]);
            String choise = (String) userChoise;
            System.out.println(choise);
            if (choise == null || choise.equals(OPTIONS[2])) {
                JOptionPane.showMessageDialog(null, "Va mai asteptam!");
                System.exit(0);
            } else if (choise.equals(OPTIONS[1])) {
                //afisam lista
                afiseazaList(catalogPersoane);
                continue;//reluam while-ul , sarim deodata la inceputul
                // lui while
            }

            String data = JOptionPane.showInputDialog(TEMPLATE);
            String[] personData = data.split(",");
            String name = personData[0];
            Date bthDate = null;
            try {
                bthDate = DateUtil.getBirthDay(personData[1]);
            } catch (Exception e) {
                //tratarea exceptiei

                JOptionPane.showMessageDialog(null,
                        "Date de intrare eronate, incercati din nou",
                         "Alert",JOptionPane.ERROR_MESSAGE);
                continue;//while de la inceput
            }
            Person person = new Person();
            person.setNume(name);
            person.setDataNasterii(bthDate);
            catalogPersoane.add(person);
        }
    }

    private static void afiseazaList(List<Person> catalogPersoane) {
        PersonUtil putil = new PersonUtil();
        putil.showAll(catalogPersoane);
    }

}
