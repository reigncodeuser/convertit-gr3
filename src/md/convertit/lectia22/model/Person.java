package md.convertit.lectia22.model;

import java.util.Date;

/**
 * Created by Utilizator on 19.12.2016.
 */
public class Person {
    private String nume;
    private Date dataNasterii;
    private StatutSocial statutSocial;

    public Person() {
    }

    public Person(String nume, StatutSocial statutSocial) {
        this.nume = nume;
        this.statutSocial = statutSocial;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Date getDataNasterii() {
        return dataNasterii;
    }

    public void setDataNasterii(Date dataNasterii) {
        this.dataNasterii = dataNasterii;
    }

    public StatutSocial getStatutSocial() {
        return statutSocial;
    }

    public void setStatutSocial(StatutSocial statutSocial) {
        this.statutSocial = statutSocial;
    }

    @Override
    public String toString() {
        return "Person{" +
                "nume='" + nume + '\'' +
                ", dataNasterii=" + dataNasterii +
                ", statutSocial=" + statutSocial +
                '}';
    }
}
