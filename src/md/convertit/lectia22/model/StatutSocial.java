package md.convertit.lectia22.model;

/**
 * Created by Utilizator on 19.12.2016.
 */
public enum StatutSocial {
    /**
     * persoana necasatorita
     */
    CELIBATAR,
    CASATORIT,
    CONCUBINAJ
}
