package md.convertit.lectia22.programs;


import md.convertit.lectia22.model.Person;
import md.convertit.lectia22.model.StatutSocial;
import md.convertit.lectia22.util.DateUtil;
import md.convertit.lectia22.util.PersonUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Utilizator on 19.12.2016.
 */
public class StareCivilaRegistru {
    public static void main(String[] args) throws ParseException {
        //declaram un obiect de tip Person
        Person p1;
        //initializam folosind constructorul cu 0 parametri
        p1 = new Person();
        //setam nume,
        p1.setNume("Denis Chiosa");
        //setam statut social
        p1.setStatutSocial(StatutSocial.CONCUBINAJ);
        //declaram un obiect de tip Date
        Date bthDate;
        bthDate = DateUtil.getBirthDay("13/03/1988");
        p1.setDataNasterii(bthDate);
        System.out.println(p1);

        List<Person> personList = new ArrayList<>(5);//5 marimea initiala
        System.out.println("este lista goala: " + personList.isEmpty());
        //adaugam pe p1 in lista
        personList.add(p1);
        //mai adaugam 2 persoane
        p1 = new Person("Ion Posmag", StatutSocial.CELIBATAR);
        p1.setDataNasterii(DateUtil.getBirthDay("25/12/2000"));
        //il adaugam in lista
        personList.add(p1);
        //mai adaugam o persoana in lista
        personList.add(new Person("Maria Biesu", StatutSocial.CASATORIT));

        personList.add(new Person("Maria Besalache", StatutSocial.CONCUBINAJ));
        personList.add(new Person("Igor Carsan", StatutSocial.CELIBATAR));
        personList.add(new Person("Gheorghe Palaghioi", StatutSocial.CELIBATAR));
        personList.add(new Person("Vera Bubuioc", StatutSocial.CELIBATAR));
        personList.add(new Person("Vasile Scafarlie", StatutSocial.CELIBATAR));
        personList.add(new Person("Nicolae Chiosa", StatutSocial.CASATORIT));
        personList.add(new Person("Sergiu Metgher", StatutSocial.CONCUBINAJ));
        System.out.println("total in lista avem " + personList.size() + " persoane");

        //afisez toate persoanele din lista
        //PersonUtil.showAll(personList); showAll este o variabila de instanta
        // (de obiect), si trebuie sa cream un obiect al clasei PersonUtil
        PersonUtil personUtil = new PersonUtil();
        personUtil.showAll(personList);

        List<Person> listaCasatoriti = personUtil
                .getAllByStatut(personList, StatutSocial.CASATORIT);
        //afisez lista de persoane casatorite
        System.out.println("\n\n");
        personUtil.showAll(listaCasatoriti);
        //filtram intro lista noua toate persoanele Celibatare
        List<Person> listaCelibatari = personUtil.getAllByStatut(personList, StatutSocial.CELIBATAR);
        System.out.println("\n\n Celibatari");
        personUtil.showAll(listaCelibatari);
    }
}
