package md.convertit.lectia22.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Utilizator on 19.12.2016.
 */
public class DateUtil {

    public static Date getBirthDay(String stringBthDay) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = format.parse(stringBthDay);
        return date;
    }
}
