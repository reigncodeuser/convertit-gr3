package md.convertit.lectia22.util;

import md.convertit.lectia22.model.Person;
import md.convertit.lectia22.model.StatutSocial;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 19.12.2016.
 */
public class PersonUtil {

    //declaram o metoda care afiseaza toate persoanele dintr-o lista
    public void showAll(List<Person> personList) {
        System.out.println("Incepem sa afisam toate persoanele");
        for (Person person : personList) {
            System.out.println(person);
        }
        System.out.println("Am afisat toate persoanele");
    }

    public List<Person> getAllByStatut(List<Person> personList,
                                       StatutSocial statutSocial) {
        List<Person> filteredList = new ArrayList<>();//gatim lista goala
        //iteram prin toate obiectele
        for (Person person : personList) {
            //verific daca are statut social transmis, si il adaug in
            //lista :filteredList
            if (person.getStatutSocial() == statutSocial) {
                filteredList.add(person);
            }
        }
        return filteredList;
    }
}
