package md.convertit.lectia23;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Utilizator on 21.12.2016.
 */
public class HashMapExample {

    public static void main(String[] args) {
        Map<String, String> myMap = new HashMap<>();
        myMap.put("BOS300", "Opel Vectra");
        System.out.println("marime map: " + myMap.size());
        System.out.println(myMap);
        myMap.put("C1", "VW PASSAT");
        myMap.put("BOS333", "Renault Megane");
        System.out.println("marime map: " + myMap.size());
        System.out.println(myMap);
        myMap.put("BOS300", new String("Mercedes"));
        System.out.println(myMap);
        myMap.remove("C1");
        System.out.println(myMap);

        System.out.println("****************");

        String nrInmatriculare = "BOS300";
        String car = myMap.get(nrInmatriculare);
        System.out.println("pentru nr: " + nrInmatriculare
                + " avem masina " + car);

        nrInmatriculare = "BG ZTE002";
        car = myMap.get(nrInmatriculare);
        System.out.println("pentru nr: " + nrInmatriculare
                + " avem masina " + car);

        Set<String> mapKeys = myMap.keySet();
        for (String key : mapKeys) {
            System.out.println("keia " + key + " are valoare: "
                    + myMap.get(key));
        }
    }
}
