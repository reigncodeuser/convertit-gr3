package md.convertit.lectia24.model;

/**
 * Created by Utilizator on 23.12.2016.
 */
public class Car {
    private String model;
    private int year;
    private Color color;
    private String plateNumber;

    public Car() {
    }

    public Car(String model, int year, Color color, String plateNumber) {
        this.model = model;
        this.year = year;
        this.color = color;
        this.plateNumber = plateNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return plateNumber != null ? plateNumber.equals(car.plateNumber) : car.plateNumber == null;

    }

    @Override
    public int hashCode() {
        return plateNumber != null ? plateNumber.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", year=" + year +
                ", color=" + color +
                ", plateNumber='" + plateNumber + '\'' +
                '}';
    }
}
