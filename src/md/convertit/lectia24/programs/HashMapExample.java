package md.convertit.lectia24.programs;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by Utilizator on 23.12.2016.
 */
public class HashMapExample {
    public static void main(String[] args) {
//        Map<String,String> myMap = new HashMap<>();
        Map<String,String> myMap = new TreeMap<>();
        myMap.put("BOS300","Opel Vectra");
        myMap.put("XXX123","WP Passat");
        myMap.put("AUD000","Audi A5");
        myMap.put("AUD000","Dacia Logan");

        //citesc valorile din map (.get())
        String value = myMap.get("BOS300");
        System.out.println("BOS300 este: " + value);

        System.out.println("Contine AUD000? " + myMap.containsKey("AUD000"));
        System.out.println("Contine BAD222? " + myMap.containsKey("BAD222"));

        Set<String> allMapKeys = myMap.keySet();
        //afisam toate cheile
        for(String key : allMapKeys){
            System.out.println("key is: " + key);
        }
        System.out.println("*************");
        //afisam toate valorile folosind cheile
        for(String key : allMapKeys){
            System.out.println("key is: " + key
                    + " value is " + myMap.get(key));
        }


    }
}
