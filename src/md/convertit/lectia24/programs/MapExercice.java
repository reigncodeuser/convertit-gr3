package md.convertit.lectia24.programs;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Utilizator on 23.12.2016.
 */
public class MapExercice {
    public static void main(String[] args) {
        String text = "Poimiine vine craciunul, iiiho-ho-ho";
        Map<Character, Integer> repetariMap = new HashMap<>();
        repetariMap.put('i', 0);
        repetariMap.put('n', 0);
        for (int index = 0; index < text.length(); index++) {
            char currentChar = text.charAt(index);
            if (currentChar == 'i') {
                int repetariCurente = repetariMap.get('i');
                repetariMap.put('i',++repetariCurente);
            }else if(currentChar=='n'){
                int repetariCurente = repetariMap.get('n');
                repetariMap.put('n',++repetariCurente);
            }
        }//am terminat de numarat
        //aratam repetarile
        int totalRepetari = repetariMap.get('i');
        int totalRepetariN = repetariMap.get('n');
        System.out.println("i se repeta de " + totalRepetari + " ori");
        System.out.println("n se repeta de " + totalRepetariN + " ori");
    }
}
