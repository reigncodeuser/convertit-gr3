package md.convertit.lectia24.programs;

import md.convertit.lectia24.model.Car;
import md.convertit.lectia24.model.Color;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Utilizator on 23.12.2016.
 */
public class SetExample {
    public static void main(String[] args) {
        //cream un Set de tip String
        Set<String> weekDays = new HashSet<>();
        System.out.println("este gol: " + weekDays.isEmpty());
        weekDays.add("Luni");
        weekDays.add("Marti");
        weekDays.add("Marti");
        System.out.println("wekdays are marimea "+ weekDays.size());
        System.out.println("****************************");

        //cream un Set de tip Car
        Set<Car> carSet = new HashSet<>();
        carSet.add(new Car("Vectra",1976, Color.YELLOW,"BOS300"));
        carSet.add(new Car("opel vectra",1976, Color.YELLOW,"BOS300"));
        carSet.add(new Car("opel vectra",1976, Color.YELLOW,"CAP555"));
        System.out.println("carSet are marimea: " + carSet.size());

        Car c1 = new Car();
        c1.setPlateNumber("YYY200");

        Car c2 = new Car();
        c2.setPlateNumber("TOC111");

        System.out.println("c1 == cu c2 ?? " + c1.equals(c2));

        System.out.println(c1); //automat apeleaza .toString()
        System.out.println(c2.toString());
    }
}
