package md.convertit.lectia25.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Utilizator on 09.01.2017.
 */
public class MyFirstFrame extends JFrame {
    // declaram componentele la nivel de clasa
    // ca mai tirziu sa avem acces la ele
    private JPanel panel;
    private JButton button;
    private JLabel label;

    public MyFirstFrame() {
        initUI();
        addActions();
    }

    // Adauga actine la componente
    private void addActions() {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String textButton = button.getText();
                if(textButton.equalsIgnoreCase("exit")){
                    System.exit(1);
                }

                String textDePeLabel = label.getText();
                int valoareInt = Integer.parseInt(textDePeLabel);
                valoareInt++;
                label.setText(String.valueOf(valoareInt));
                //verificam daca numarul de click-uri nu
                //este mai mare ca 10
                if (valoareInt == 10) {
                    JOptionPane.showMessageDialog(MyFirstFrame.this,
                            "Ati depasit numarul de click-uri admise");
                    button.setText("Exit");
                }

            }
        });
    }

    /**
     * Porneste applicatia. Face JFrame-ul nostru vizibil.
     */
    public void start() {
        setVisible(true);
    }

    /**
     * Initializaza interfata grafica a programului
     */
    private void initUI() {
        // initializare MyFirstFrame
        setTitle("My First App");
        setSize(600, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // initializam panel
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.lightGray);

        // initializam buton si label. Adaugam pe rind pe panel. Adaugam
        // panelul pe frame
        button = new JButton("Apasa-ma");
        label = new JLabel("0");
        label.setForeground(Color.BLUE);
        panel.add(button);
        panel.add(label);
        add(panel);

         button.setBounds(100, 50, 100, 200);
// Medoda de aranjare Absolute Positioning.
// Aranjam componentele pe coordonate manual.
    }
}
