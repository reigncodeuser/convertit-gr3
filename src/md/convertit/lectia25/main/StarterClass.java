package md.convertit.lectia25.main;

import md.convertit.lectia25.gui.MyFirstFrame;

public class StarterClass {

    public static void main(String[] args) {
        MyFirstFrame frame = new MyFirstFrame();
        frame.start();
    }
}
