package md.convertit.lectia26.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Utilizator on 11.01.2017.
 */
public class LayoutFrame extends JFrame {

    private JPanel panel = new JPanel();
    private JButton button1 = new JButton("Button 1");
    private JButton button2 = new JButton("Button 2");
    private JButton button3 = new JButton("Button 3");
    private JButton button4 = new JButton("Button 4");
    private JButton button5 = new JButton("Button 5");

    public LayoutFrame() {
        setSize(600, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().add(panel);
    }

    public void addWithFlowLayoutLeft(){
        // cream flow layout
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        // adauga spatiu pe orizontal si vertical
        layout.setHgap(20);
        layout.setVgap(20);
        // setam layoutul panelului
        panel.setLayout(layout);
        // adaugam butoanele pe panel
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
    }
    public void addWithFlowLayoutRight(){
        panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
    }
    public void addWithFlowLayoutCenter(){
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
    }

    public void addWithBoxLayoutY(){
        BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layout);
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
    }
    public void addWithBoxLayoutX(){
        BoxLayout layout = new BoxLayout(panel, BoxLayout.X_AXIS);

        panel.setLayout(layout);
        panel.add(button1);
        panel.add(Box.createRigidArea(new Dimension(20, 20)));
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
    }

    public void addWithBorderLayout(){
        BorderLayout layout = new BorderLayout();
        panel.setLayout(layout);
        panel.add(button1, BorderLayout.NORTH);
        panel.add(button2, BorderLayout.CENTER);
        panel.add(button3, BorderLayout.SOUTH);
        panel.add(button4, BorderLayout.EAST);
        panel.add(button5, BorderLayout.WEST);
    }

    public void addWithGridLayout(){
        // 2 - rinduri  3 - coloane  10 - spatiu orizontal
        // 15 - spatiu vertical
        GridLayout layout = new GridLayout(2, 3, 10, 15);
        panel.setLayout(layout);
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.add(button4);
        panel.add(button5);
    }
}
