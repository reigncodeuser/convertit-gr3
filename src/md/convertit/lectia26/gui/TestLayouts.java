package md.convertit.lectia26.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Utilizator on 11.01.2017.
 */
public class TestLayouts extends JFrame {

    private JPanel mainPanel;

    public TestLayouts() {
        setTitle("Test Layouts");
        setSize(600, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    // Porneste applicatia
    public void start() {
        init();
        setVisible(true);
    }

    // initializeaza toate componenetele
    private void init() {
        initMainPanel();
        initTopPanel();
        initLeftPanel();
        initBottomPanel();
        initCenterPanel();

        getContentPane().add(mainPanel);
    }

    private void initCenterPanel() {
        JPanel centerPanel = new JPanel();
        // 2, 2 -rows si columns
        // 10, 10 - spatiu orizontal si vertical
        GridLayout layout = new GridLayout(2, 2, 10, 10);
        centerPanel.setLayout(layout);
        mainPanel.add(centerPanel, BorderLayout.CENTER);

        centerPanel.add(new JButton("B1"));
        centerPanel.add(new JButton("B2"));
        centerPanel.add(new JButton("B3"));
        centerPanel.add(new JButton("B4"));
        centerPanel.setBackground(Color.lightGray);
    }

    private void initBottomPanel() {
        JPanel bottomPanel = new JPanel(
                new FlowLayout(FlowLayout.CENTER));
        bottomPanel.add(new JLabel("2017"));
        mainPanel.add(bottomPanel, BorderLayout.SOUTH);
    }

    private void initLeftPanel() {
        JPanel leftPanel = new JPanel();
        BoxLayout layout = new BoxLayout(leftPanel, BoxLayout.Y_AXIS);
        leftPanel.setLayout(layout);
        leftPanel.add(new JLabel("Item 1"));
        leftPanel.add(new JLabel("Item 2"));
        leftPanel.add(new JLabel("Item 3"));
        leftPanel.setBackground(Color.yellow);
        mainPanel.add(leftPanel, BorderLayout.WEST);
    }

    // Initializam principalul panel si ii setam BorderLayout
    private void initMainPanel() {
        mainPanel = new JPanel();
        BorderLayout layout = new BorderLayout();
        mainPanel.setLayout(layout);
        mainPanel.setBackground(Color.red);
    }

    private void initTopPanel() {
        JPanel topPanel = new JPanel();
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        layout.setHgap(15); // spatiu pe orizontal intre componente
        topPanel.setLayout(layout);
        topPanel.add(new JLabel("Menu1"));
        topPanel.add(new JLabel("Menu2"));
        topPanel.add(new JLabel("Menu3"));

        mainPanel.add(topPanel,BorderLayout.NORTH);
    }


}
