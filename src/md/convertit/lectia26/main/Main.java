package md.convertit.lectia26.main;

import md.convertit.lectia26.gui.LayoutFrame;

/**
 * Created by Utilizator on 11.01.2017.
 */
public class Main {
    public static void main(String[] args) {
        LayoutFrame frame = new LayoutFrame();
        frame.setVisible(true);

        // <<<<FlowLayout>>>>
//        frame.addWithFlowLayoutLeft();
//        frame.addWithFlowLayoutRight();
//        frame.addWithFlowLayoutCenter();

        // <<<<BoxLayout>>>>
//        frame.addWithBoxLayoutX();
//        frame.addWithBoxLayoutY();

        // <<<<BorderLayout>>>>
//        frame.addWithBorderLayout();

        // <<<<GridLayout>>>>
        frame.addWithGridLayout();
    }
}
