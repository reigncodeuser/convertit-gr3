package md.convertit.lectia27.gui;

import md.convertit.lectia27.util.MenuUtil;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Utilizator on 13.01.2017.
 */
public class CalculatorFrame extends JFrame {

    private JTextField cifra1JTextField;
    private JTextField cifra2JTextField;
    private int selected = 1;
    private JComboBox<String> operationJComboBox;
    private JLabel rezultatJLabel;
    private JCheckBox jCheckBoxRed;
    private JRadioButton jRadioButtonBold;
    private JRadioButton jRadioButtonRegular;
    private MyButtonListener myButtonListener;

    public CalculatorFrame() {
        setSize(600, 400);
        setTitle("Calculator");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    // metoda de start a calculatorului
    public void start() {
        init();
        setVisible(true);
    }

    // initializare componente jframe
    private void init() {
        // initializat mainPanel
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(initOperationPanel(), BorderLayout.NORTH);

        // initializare panel cu rezultatul
        mainPanel.add(initRezultatPanel(), BorderLayout.SOUTH);

        // Cream si adaugam panelul cu numere pe mainPanel
        JPanel panelNumeric = crearePanelCuNumere();
        mainPanel.add(panelNumeric, BorderLayout.CENTER);

        // Cream meniul si il adaugam pe frame
        createMenu();

        // adaugam mainPanel pe CalculatorFrame
        add(mainPanel);
    }

    // metoda care va crea meniul si va seta frame-ului
    private void createMenu() {
        // Cream MenuUtil
        MenuUtil menuUtil = new MenuUtil(myButtonListener);
        JMenuBar menuBar = menuUtil.createMenu(false);
        // adaugam menuBar pe frame
        setJMenuBar(menuBar);
    }

    // Metoda de initiere a panulului cu numere.
    private JPanel crearePanelCuNumere() {
        JPanel panel = new JPanel();
        GridLayout gridLayout = new GridLayout(4, 3, 10, 10);
        panel.setLayout(gridLayout);

        // initializam listenerul nostru pentru butoane
        myButtonListener = new MyButtonListener(this);

        // Cream butoanele si ai setam listenerul
        JButton b7 = new JButton("7");
        b7.addActionListener(myButtonListener);
        panel.add(b7);

        JButton b8 = new JButton("8");
        b8.addActionListener(myButtonListener);
        panel.add(b8);

        JButton b9 = new JButton("9");
        b9.addActionListener(myButtonListener);
        panel.add(b9);

        JButton b4 = new JButton("4");
        b4.addActionListener(myButtonListener);
        panel.add(b4);

        JButton b5 = new JButton("5");
        b5.addActionListener(myButtonListener);
        panel.add(b5);

        JButton b6 = new JButton("6");
        b6.addActionListener(myButtonListener);
        panel.add(b6);

        JButton b1 = new JButton("1");
        b1.addActionListener(myButtonListener);
        panel.add(b1);

        JButton b2 = new JButton("2");
        b2.addActionListener(myButtonListener);
        panel.add(b2);

        JButton b3 = new JButton("3");
        b3.addActionListener(myButtonListener);
        panel.add(b3);

        JButton bC = new JButton("C");
        bC.addActionListener(myButtonListener);
        panel.add(bC);

        JButton b0 = new JButton("0");
        b0.addActionListener(myButtonListener);
        panel.add(b0);

        JButton bE = new JButton("=");
        bE.addActionListener(myButtonListener);
        panel.add(bE);

        return panel;
    }

    // Metoda de initiare panel cu rezultatJLabel
    private JPanel initRezultatPanel() {
        JPanel panelRezultat = new JPanel();
        rezultatJLabel = new JLabel("Aici o sa apara rezultat");
        rezultatJLabel.setFont(new Font("Arial", Font.PLAIN, 20));
        panelRezultat.add(rezultatJLabel);
        panelRezultat.setBorder(new SoftBevelBorder(BevelBorder.LOWERED));

        JPanel panelInstrumente = createPanelDeInstrumente();

        JPanel panel = new JPanel();
        BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layout);
        panel.add(panelRezultat);
        panel.add(panelInstrumente);
        return panel;
    }

    // metoda de creare a panelului de instrumente
    private JPanel createPanelDeInstrumente() {
        // Initializam jCheckBoxRed
        jCheckBoxRed = new JCheckBox("RED");
        jCheckBoxRed.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                boolean rezultat = jCheckBoxRed.isSelected();
                if (rezultat) {
                    rezultatJLabel.setForeground(Color.RED);
                } else {
                    rezultatJLabel.setForeground(Color.BLACK);
                }
            }
        });

        // Initializam ButtonGroup with RadioButtons
        ButtonGroup buttonGroup = new ButtonGroup();
        jRadioButtonBold = new JRadioButton("Bold");
        jRadioButtonBold.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rezultatJLabel.setFont(new Font("Arial", Font.BOLD, 20));
            }
        });

        jRadioButtonRegular = new JRadioButton("Regular");
        jRadioButtonRegular.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rezultatJLabel.setFont(new Font("Arial", Font.PLAIN, 20));
            }
        });
        jRadioButtonRegular.setSelected(true);

        buttonGroup.add(jRadioButtonBold);
        buttonGroup.add(jRadioButtonRegular);

        // Initiez panelul pentru instrumente si adaug instrumentele pe el
        JPanel panel = new JPanel();
        panel.add(jCheckBoxRed);
        panel.add(jRadioButtonBold);
        panel.add(jRadioButtonRegular);

        JSlider sizeSlider = new JSlider(JSlider.HORIZONTAL, 10, 30, 20);
        sizeSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                int size = source.getValue();
                rezultatJLabel.setFont(new Font("Arial", Font.BOLD, size));
            }
        });
        panel.add(sizeSlider);
        return panel;
    }

    // initializam panelul cu cifra1JTeftField, cifra2JTeftField,
    // operationJComboBox
    private JPanel initOperationPanel() {
        // initializare panel cu FlowLayout.CENTER
        JPanel panel = new JPanel();
        FlowLayout layout = new FlowLayout(FlowLayout.CENTER);
        layout.setHgap(10);
        layout.setVgap(5);
        panel.setLayout(layout);

        // initializam cifra1JTeftField si adaugam pe panel
        cifra1JTextField = new JTextField(7);
        cifra1JTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                selected = 1;
            }
        });
        panel.add(cifra1JTextField);

        // initializam operationJComboBox
        initOperationJComboBox();
        panel.add(operationJComboBox);

        // initializam cifra2JTeftField si adaugam pe panel
        cifra2JTextField = new JTextField(7);
        cifra2JTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                selected = 2;
            }
        });
        panel.add(cifra2JTextField);

        // adaugam border la panel
        panel.setBorder(new EtchedBorder());
        return panel;
    }

    // Initializam JComboBox
    private void initOperationJComboBox() {
        String[] operatii = {"+", "-", "*", "/"};
        operationJComboBox = new JComboBox<>(operatii);
        operationJComboBox.setPreferredSize(new Dimension(50, 19));
    }

    public JTextField getCifra1JTextField() {
        return cifra1JTextField;
    }

    public JTextField getCifra2JTextField() {
        return cifra2JTextField;
    }

    public int getSelected() {
        return selected;
    }

    public JComboBox<String> getOperationJComboBox() {
        return operationJComboBox;
    }

    public JLabel getRezultatJLabel() {
        return rezultatJLabel;
    }
}
