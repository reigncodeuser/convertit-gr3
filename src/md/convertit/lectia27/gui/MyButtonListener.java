package md.convertit.lectia27.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Utilizator on 16.01.2017.
 */
public class MyButtonListener implements ActionListener {

    private CalculatorFrame parentFrame;

    public MyButtonListener(CalculatorFrame parentFrame) {
        this.parentFrame = parentFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();
        System.out.println(button.getText());

        // obtin textul de pe duttonul apasat
        String pressedButton = button.getText();
        // verific daca este C sau =
        if(pressedButton.equals("C")){
            // stergem textul din cifra1JTextField si cifra2JTextField
            stergeRezultat();
        } else if(pressedButton.equals("=")){
            calculeazaRezultat();
        } else {
            String numar = "";
            if (parentFrame.getSelected() == 1) {
                numar = parentFrame.getCifra1JTextField().getText()
                        + button.getText();
                parentFrame.getCifra1JTextField().setText(numar);
            } else {
                numar = parentFrame.getCifra2JTextField().getText()
                        + button.getText();
                parentFrame.getCifra2JTextField().setText(numar);
            }
        }
    }

    // metoda sterge cifrele
    public void stergeRezultat() {
        parentFrame.getCifra1JTextField().setText("");
        parentFrame.getCifra2JTextField().setText("");
        parentFrame.getRezultatJLabel().setText("0");
    }

    // calculeaza rezultatul operatiei cifrelor
    public void calculeazaRezultat() {
        String cifra1Str = parentFrame.getCifra1JTextField().getText();
        String cifra2Str = parentFrame.getCifra2JTextField().getText();
        // rezultat = (conditie) : valoarePentruTrue ? valoarePentruFalse
        Integer cifra1 = (cifra1Str.isEmpty()) ? 0 :
                Integer.parseInt(cifra1Str);
        Integer cifra2 = (cifra2Str.isEmpty()) ? 0 :
                Integer.parseInt(cifra2Str);

        System.out.println("CIFRA 1 = " + cifra1);
        System.out.println("CIFRA 2 = " + cifra2);

        // obtinem operatia din operationJComboBox
        String operation = (String) parentFrame.getOperationJComboBox()
                .getSelectedItem();
        System.out.println("OPERATIA = " + operation);

        //Obtinem rezultatul operatiei
        String rezultat;
        switch (operation){
            case "+":{
                rezultat = String.valueOf(cifra1 + cifra2);
                break;
            }
            case "-": {
                rezultat = String.valueOf(cifra1 - cifra2);
                break;
            }
            case "*": {
                rezultat = String.valueOf(Long.valueOf(cifra1)*
                        Long.valueOf(cifra2));
                break;
            }
            case "/": {
                rezultat = String.valueOf(Double.valueOf(cifra1) /
                        Double.valueOf(cifra2));
                break;
            }
            default: rezultat = "0";
        }

        // afisam rezultatul pe rezultatJLabel
        parentFrame.getRezultatJLabel().setText(rezultat);
    }
}
