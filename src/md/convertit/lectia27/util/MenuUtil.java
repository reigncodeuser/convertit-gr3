package md.convertit.lectia27.util;

import md.convertit.lectia27.gui.MyButtonListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by Utilizator on 20.01.2017.
 */
public class MenuUtil {

    private MyButtonListener myButtonListener;

    public MenuUtil(MyButtonListener myButtonListener) {
        this.myButtonListener = myButtonListener;
    }

    public JMenuBar createMenu(boolean withTest) {
        // cream JMenuBar
        JMenuBar menuBar = new JMenuBar();

        // Cream meniu "File", "About" si "Test"
        JMenu jMenuFile = new JMenu("File");
        jMenuFile.setMnemonic('F');

        JMenu jMenuHelp = new JMenu("Help");
        jMenuHelp.setMnemonic(KeyEvent.VK_H);


        // Cream MenuItem's pentru jMenuFile si le
        // adaugam MenuItem's pe jMenuFile
        JMenuItem menuItemCalculeaza = new JMenuItem("Calculate");
        menuItemCalculeaza.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myButtonListener.calculeazaRezultat();
            }
        });
        JMenuItem menuItemSterge = new JMenuItem("Reset");
        menuItemSterge.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myButtonListener.stergeRezultat();
            }
        });
        JMenuItem menuItemExit = new JMenuItem("Exit");
        menuItemExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        jMenuFile.add(menuItemCalculeaza);
        jMenuFile.add(menuItemSterge);
        jMenuFile.add(new JSeparator());
        jMenuFile.add(menuItemExit);

        // cream si adaugam MenuItem pe jMenuAbout
        JMenuItem menuItemAbout = new JMenuItem("About");
        menuItemAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog aboutDialog = new JDialog();
                aboutDialog.setTitle("About");
                aboutDialog.setSize(400,300);
                aboutDialog.setLocationRelativeTo(null);
                aboutDialog.setModal(true);
                aboutDialog.setVisible(true);
            }
        });

        jMenuHelp.add(menuItemAbout);

        // adaugam meniuFile pe menuBar
        menuBar.add(jMenuFile);
        menuBar.add(jMenuHelp);

        if (withTest) {
            createTestMenu(menuBar);
        }

        return menuBar;
    }

    private void createTestMenu(JMenuBar menuBar) {
        JMenu jMenuTest = new JMenu("Test");
        menuBar.add(jMenuTest);

        JMenuItem menuItemCheckBox = new JCheckBoxMenuItem("Checkbox");
        jMenuTest.add(menuItemCheckBox);
        JRadioButtonMenuItem jr1 = new JRadioButtonMenuItem("Radio1");
        jr1.setSelected(true);
        JRadioButtonMenuItem jr2 = new JRadioButtonMenuItem("Radio2");

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(jr1);
        buttonGroup.add(jr2);
        jMenuTest.add(jr1);
        jMenuTest.add(jr2);

        // adaugare jMenuTestSubmenu2 in jMenuTestSubmenu1
        // adaugare jMenuTestSubmenu1 in jMenuTest
        JMenu jMenuTestSubmenu1 = new JMenu("Submenu 1");
        JMenu jMenuTestSubmenu2 = new JMenu("Submenu 2");
        jMenuTest.add(jMenuTestSubmenu1);
        jMenuTestSubmenu1.add(jMenuTestSubmenu2);
    }
}
