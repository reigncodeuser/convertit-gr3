package md.convertit.lectia28.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Utilizator on 16.01.2017.
 */
public class ButtonTest extends JFrame {

    public ButtonTest(String title) {
        super(title);
        setSize(400, 300);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        JButton jButton = new JButton("Disable");
        panel.add(jButton,BorderLayout.NORTH);
        getContentPane().add(panel);

        setVisible(true);
    }

    public static void main(String[] args) {
        new ButtonTest("Button test");
    }
}
