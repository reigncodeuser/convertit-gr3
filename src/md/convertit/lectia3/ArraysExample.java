package md.convertit.lectia3;

/**
 * Created by Utilizator on 04.11.2016.
 */
public class ArraysExample {

    public static void main(String args[]) {
        // declaram un array de int-uri
        // <tip>[] numeTablou;
        int[] note;  // sintaxa alternativa ->  int note[];

        //intializam tabloul (array-ul) cu marimea 5
        // numeTablou = new <tip>[<marimeTablou>];
        note = new int[5];

        //afisam elementul tabloului de pe index 0
        System.out.println(note[0]);
        //atribuim elementului de pe index 0 din tablou valoare 10
        note[0] = 10;
        //afisam elementul tabloului de pe index 0
        System.out.println(note[0]);

        //afisam lungimea tabloului
        System.out.println("Marime tablou: " + note.length);
        //afisam la consola valorile de pe index 1 si 2
        System.out.println(note[1]);
        System.out.println(note[2]);
        //atribuim valori pentru elementele de pe index 1,2,3,4
        //valorile: 9,25,56,12
        note[1] = 9;
        note[2] = 25;
        note[3] = 56;
        note[4] = 12;
        System.out.println(note[1]);
        System.out.println(note[2]);
        System.out.println(note[3]);
        System.out.println(note[4]);

        //atribuim pe index 5 valoare 100
        note[5]=100;
        System.out.println("Sfirsit program");
    }
}
