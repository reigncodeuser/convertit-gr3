package md.convertit.lectia3;

/**
 * Created by Utilizator on 04.11.2016.
 */
public class ArraysExample2 {

    public static void main(String[] args){
        //declaram si initializam un tablou cu valori initiale
        String[] zileSaptamina = {"luni","marti","miercuri","joi",
        "vineri","sambata"};
        //afisam la consola elementul de pe index 5
        System.out.println(zileSaptamina[5]);
        //declar o variabila de tip String cu numele 'primaZi'
        String primaZi;
        //atribui lui primaZi valoarea de pe index 0 din tablou
        primaZi = zileSaptamina[0];
        System.out.println(primaZi);
        //modificam valoarea variabile primaZi in : "LUNI"
        primaZi="LUNI";
        //afisam la consola variabila primaZi
        //afisam la consola valoarea elementului din index 0 a zileSaptama
        System.out.println(primaZi);
        System.out.println(zileSaptamina[0]);
        //afisam marimea tabloului
        System.out.println("marime tablou: " + zileSaptamina.length);
    }
}
