package md.convertit.lectia3;

/**
 * Created by Utilizator on 04.11.2016.
 */
public class ArraysExample3 {
    public static void main(String[] args) {
        //declaram si initializam un tablou cu 3 valori initiale (int)
        int[] t1 = {1,2,3};
        System.out.println(t1[0]);
        System.out.println(t1[1]);
        System.out.println(t1[2]);

        //declar un tablou de tip int, nume t2
        int[] t2;
        //atribui lui t2 variabila t1
        t2 = t1;
        t2[0] = -10;
        //afisam la consola elementul 0 din tablou t1
        System.out.println(t1[0]);

    }
}
