package md.convertit.lectia31.main;

import md.convertit.lectia31.model.Person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 23.01.2017.
 */
public class CatalogBD {

    public List<Person> getList() throws SQLException {
        //se conecteaza la bd
        Connection connection = DbUtil.getConnection();
        //executa query
        String query = "select * from personss";
      PreparedStatement ps = connection.prepareStatement(query);
        ResultSet resultSet = ps.executeQuery();
        List<Person> personList = new ArrayList<>();
        while (resultSet.next()){
            int id = resultSet.getInt("id");
           String firstName = resultSet.getString("firstname");
            String lastName = resultSet.getString("lastname");
            int year = resultSet.getInt("year");

            Person person = new Person();
            person.setId(id);
            person.setFirstName(firstName);
            person.setLastName(lastName);
            person.setYear(year);
            personList.add(person); //adaug persoana in lista
        }
        //populeaza lista
        return personList;
    }
}
