package md.convertit.lectia31.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Utilizator on 23.01.2017.
 */
public class DbUtil {

    public static Connection getConnection() {
        //incarcam driver in memorie
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("am incarcat driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //cream conexiunea
        String url = "jdbc:mysql://localhost/gr3";
        String user = "root";
        String password = "convertit";

        Connection connection = null;//declar connection
        try {//initializez
            connection =
                    DriverManager.getConnection(url, user, password);
            System.out.println("ne-am conectat cu succes");
            return  connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
