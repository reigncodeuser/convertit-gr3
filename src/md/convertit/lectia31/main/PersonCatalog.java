package md.convertit.lectia31.main;

import md.convertit.lectia31.model.Person;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Utilizator on 23.01.2017.
 */
public class PersonCatalog {

    public static void main(String[] args) throws SQLException {
        List<Person> listaPersoane ;

        CatalogBD catalog  = new CatalogBD();
        listaPersoane = catalog.getList();
        System.out.println(listaPersoane);
    }
}
