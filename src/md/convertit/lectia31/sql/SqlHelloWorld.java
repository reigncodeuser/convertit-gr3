package md.convertit.lectia31.sql;

import md.convertit.lectia31.model.Person;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 23.01.2017.
 */
public class SqlHelloWorld {
    public static void main(String[] args) {

        //incarcam driver in memorie
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("am incarcat driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //cream conexiunea
        String url = "jdbc:mysql://localhost/gr3";
        String user = "root";
        String password = "convertit";

        Connection connection = null;//declar connection
        try {//initializez
            connection =
                    DriverManager.getConnection(url, user, password);
            System.out.println("ne-am conectat cu succes");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //pregatim SQL Query
        String query = "INSERT INTO `gr3`.`personss` (`firstname`, `lastname`, `year`) VALUES (?, ?, ?);";
        try {

            String firstName = JOptionPane.showInputDialog("Prenumele va rog?");
            String lastName = JOptionPane.showInputDialog("Numele va rog?");
            int year = Integer.parseInt(JOptionPane.showInputDialog("An nastere?"));

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, firstName);
            ps.setString(2, lastName);
            ps.setInt(3, year);

            ps.execute();
            JOptionPane.showMessageDialog(null, "salvat cu success");

            //citim rezultatele din baza de date
            query = "select * from personss";
            ps = connection.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            List<Person> personList = new ArrayList<>();
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                firstName = resultSet.getString("firstname");
                lastName = resultSet.getString("lastname");
                year = resultSet.getInt("year");

                Person person = new Person();
                person.setId(id);
                person.setFirstName(firstName);
                person.setLastName(lastName);
                person.setYear(year);
                personList.add(person); //adaug persoana in lista
            }

            System.out.println(personList);
            //inchidem conexiune la BD
            connection.close();
            System.out.println("am inchis conexiunea");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
