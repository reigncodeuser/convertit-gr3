package md.convertit.lectia32.io;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Utilizator on 25.01.2017.
 */
public class CatalogEmployee {

    public static void main(String[] args) throws IOException {
        String PATH = "catalog.csv";
        File file = new File(PATH);
        FileWriter fileWriter = new FileWriter(file, true);

        StringBuilder builder;
        while (true) {
            builder = new StringBuilder();

            String name = JOptionPane.showInputDialog("Name please");
            double salary =
                    Double.parseDouble(JOptionPane.showInputDialog("Salary?"));
            String department = JOptionPane.showInputDialog("Departmen ?");

            builder.append(name).append(',').append(salary)
                    .append(',').append(department).
                    append(System.lineSeparator());
            fileWriter.write(builder.toString());

            int result = JOptionPane.showConfirmDialog(null,
                    "Continuati ?", "Mesaj", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.NO_OPTION) {
                fileWriter.close();
                JOptionPane.showMessageDialog(null, "vezi lista la " + file.getAbsolutePath());
                break;
            }
        }

    }
}
