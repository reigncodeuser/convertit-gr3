package md.convertit.lectia32.io;

import java.io.*;

/**
 * Created by Utilizator on 25.01.2017.
 */
public class FileReaderExample {

    public static void main(String[] args) throws IOException {
        final String PATH = "testfile.txt";
        File file = new File(PATH);
        FileReader reader = new FileReader(file);
        BufferedReader bf = new BufferedReader(reader);
        String line = bf.readLine();
        while ( line!=null ){
            System.out.println(line);
            line = bf.readLine();
        }
    }
}
