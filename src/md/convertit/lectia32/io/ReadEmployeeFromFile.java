package md.convertit.lectia32.io;

import md.convertit.lectia32.model.Employee;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 25.01.2017.
 */
public class ReadEmployeeFromFile {
    public static void main(String[] args) throws IOException {

        final String PATH = "employees.csv";
        File file = new File(PATH);

        BufferedReader bf = new BufferedReader(new FileReader(file));
        List<Employee> employeeList = new ArrayList<>();

        String line = bf.readLine();

        while (line != null) {
            String[] data = line.split(",");
            Employee e = new Employee();
            e.setName(data[0]);
            e.setSalary(Double.parseDouble(data[1]));
            e.setDepartment(data[2]);

            //adaugam employee in lista
            employeeList.add(e);

            line = bf.readLine();
        }
        bf.close();
        System.out.println("Total cititi din fisier avem :" + employeeList.size());
        System.out.println(employeeList);
    }
}
