package md.convertit.lectia32.io;

import md.convertit.lectia32.model.Employee;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Utilizator on 25.01.2017.
 */
public class SaveEmployeeToFile {
    //declaram si initilizam un array de Angajati
    static final Employee[] employees = new Employee[]{
            new Employee("Denis", 250000, "Parlament"),
            new Employee("Petru", 46000, "Padurar"),
            new Employee("Ileana", 18000, "Resurse umane"),
    };

    public static void main(String[] args) throws IOException {

        final String PATH = "employees.csv";
        File file = new File(PATH);
        FileWriter fileWriter = new FileWriter(file, true);

        StringBuilder builder;

        for (int i = 0; i < employees.length; i++) {
            builder = new StringBuilder();
            Employee e = employees[i];
            builder.append(e.getName());
            builder.append(',');
            builder.append(e.getSalary());
            builder.append(',');
            builder.append(e.getDepartment());
            builder.append("\n");
            fileWriter.write(builder.toString());
        }
        fileWriter.close();
        System.out.println("Informatii salvate cu success in fisier: "
                + file.getAbsolutePath());
    }
}
