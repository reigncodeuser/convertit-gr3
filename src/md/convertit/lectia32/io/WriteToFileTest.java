package md.convertit.lectia32.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Utilizator on 25.01.2017.
 */
public class WriteToFileTest {

    public static void main(String[] args) {
        final String MY_PATH = "testfile.txt";
        File file = new File(MY_PATH);
        System.out.println("fisierul are marimea " + file.length());
        //scriere in fisier se face cu clasa FileWriter

        try {
            FileWriter fileWriter = new FileWriter(file,true);
            fileWriter.write("\nalta fraza");
            fileWriter.close();

            System.out.println("fisierul are marimea " + file.length());
        } catch (IOException e) {
            System.err.println("Eroare deschidere fisier: " + e.getMessage());
        }
    }

}
