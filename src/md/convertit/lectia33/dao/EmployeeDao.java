package md.convertit.lectia33.dao;

import md.convertit.lectia32.model.Employee;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Created by Utilizator on 27.01.2017.
 */
public interface EmployeeDao {

    void save(List<Employee> list, String path) throws IOException;

    List<Employee> read(String path) throws IOException;
}
