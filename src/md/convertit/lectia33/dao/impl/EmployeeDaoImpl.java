package md.convertit.lectia33.dao.impl;

import md.convertit.lectia32.model.Employee;
import md.convertit.lectia33.dao.EmployeeDao;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 27.01.2017.
 */
public class EmployeeDaoImpl implements EmployeeDao{

    @Override
    public void save(List<Employee> list, String path) throws IOException {
        File file = new File(path);
        FileWriter fileWriter = new FileWriter(file);

        StringBuilder builder;

        for (int i = 0; i < list.size(); i++) {
            builder = new StringBuilder();
            Employee e = list.get(i);
            builder.append(e.getName());
            builder.append(',');
            builder.append(e.getSalary());
            builder.append(',');
            builder.append(e.getDepartment());
            builder.append("\n");
            fileWriter.write(builder.toString());
        }
        fileWriter.close();
        System.out.println("Informatii salvate cu success in fisier: "
                + file.getAbsolutePath());
    }

    @Override
    public List<Employee> read(String path) throws IOException {
        File file = new File(path);

        BufferedReader bf = new BufferedReader(new FileReader(file));
        List<Employee> employeeList = new ArrayList<>();

        String line = bf.readLine();

        while (line != null) {
            String[] data = line.split(",");
            Employee e = new Employee();
            e.setName(data[0]);
            e.setSalary(Double.parseDouble(data[1]));
            e.setDepartment(data[2]);

            //adaugam employee in lista
            employeeList.add(e);

            line = bf.readLine();
        }
        bf.close();
        return employeeList;
    }
}
