package md.convertit.lectia33.gui;

import md.convertit.lectia32.model.Employee;
import md.convertit.lectia33.dao.EmployeeDao;
import md.convertit.lectia33.dao.impl.EmployeeDaoImpl;
import md.convertit.lectia33.gui.model.EmployeeTableModel;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 27.01.2017.
 */
public class EmployeeCatalog extends JFrame {

    private JSplitPane jSplitPane;
    private JTextField nameTextField;
    private JSpinner salarySpinner;
    private JComboBox<String> departCombo;
    private JTable table;
    private JButton saveButton;
    private JButton deleteButton;

    public void start() {
        setTitle("Employee Catalog");
        setSize(700, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
        setVisible(true);
    }

    private void init() {
        jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPane.setDividerLocation(0.4);
        getContentPane().add(jSplitPane);
        createMenuBar();
        createFormPanel();
        createTable();
    }

    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem openMenuItem = new JMenuItem("Open");
        JMenuItem saveMenuItem = new JMenuItem("Save");
        JMenuItem exitMenuItem = new JMenuItem("Exit");

        setJMenuBar(menuBar);
        menuBar.add(fileMenu);
        fileMenu.add(openMenuItem);
        fileMenu.add(saveMenuItem);
        fileMenu.add(new JSeparator());
        fileMenu.add(exitMenuItem);

        // add action listeners
        openMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFile();
            }
        });
        saveMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFile();
            }
        });
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    private void createFormPanel() {
        JPanel panel = new JPanel();
        BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layout);

        // nameTextField
        panel.add(new JLabel("Name"));
        nameTextField = new JTextField(10);
        nameTextField.setToolTipText("Name");
        panel.add(nameTextField);
        // setam latimea la textfield maximum posibil
        // dar inaltimea setam cit este preferabil pentru un textfield
        nameTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE,
                nameTextField.getPreferredSize().height));

        // salarySpinner
        panel.add(new JLabel("Salary"));
        SpinnerNumberModel spinnerNumberModel =
                new SpinnerNumberModel(1950, 1950, 1000000, 100);
        salarySpinner = new JSpinner(spinnerNumberModel);
        salarySpinner.setMaximumSize(new Dimension(Integer.MAX_VALUE,
                salarySpinner.getPreferredSize().height));
        panel.add(salarySpinner);

        // departCombo
        panel.add(new JLabel("Department"));
        departCombo = new JComboBox<>(new String[]{"Developer", "Engineer",
                "Tester", "Sales Man"});
        departCombo.setMaximumSize(new Dimension(Integer.MAX_VALUE,
                departCombo.getPreferredSize().height));
        panel.add(departCombo);

        // saveButton
        saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveEmployee();
            }
        });

        // deleteButton
        deleteButton = new JButton("Delete");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteEmployees();
            }
        });

        JPanel jPanelButtons = new JPanel();
        jPanelButtons.add(saveButton);
        jPanelButtons.add(deleteButton);
        panel.add(jPanelButtons);

        jSplitPane.setLeftComponent(panel);
    }

    private void deleteEmployees() {
        int[] selectedRows = table.getSelectedRows();
        EmployeeTableModel model = (EmployeeTableModel) table.getModel();
        model.deleteEmployees(selectedRows);
    }

    private void saveEmployee() {
        Employee employee = new Employee();
        //1 obtin textul introdus de  user
        String name= nameTextField.getText();
        //setez textul in obiectul employee
        employee.setName(name);

        Integer salaryInput = (Integer) salarySpinner.getValue();
        employee.setSalary(salaryInput);

        String departemntInput = (String) departCombo.getSelectedItem();
        employee.setDepartment(departemntInput);

        //obtine model pentru tabel
        EmployeeTableModel model = (EmployeeTableModel) table.getModel();
        model.addEmployee(employee);
    }

    private void createTable() {
        table = new JTable();
//        table.setAutoCreateRowSorter(true);
        EmployeeTableModel model = new EmployeeTableModel();
        table.setModel(model);
        JScrollPane scrollPane = new JScrollPane(table);
        jSplitPane.setRightComponent(scrollPane);
    }

    private void saveFile() {
        JFileChooser fileChooser = new JFileChooser();
        int rezult = fileChooser.showSaveDialog(this);
        if(rezult==JFileChooser.APPROVE_OPTION){
            List<Employee> employeeList = new ArrayList<>();
            TableModel tableModel = table.getModel();
            EmployeeTableModel employeeTableModel = (EmployeeTableModel) tableModel;
            employeeList = employeeTableModel.getData();

            File file = fileChooser.getSelectedFile();
            EmployeeDao employeeDao = new EmployeeDaoImpl();
            try {
                employeeDao.save(employeeList, file.getAbsolutePath());
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this,
                        "Invalid path. Try again.");
            }
        }
    }

    private void openFile() {
               //pregatim file chooser
        JFileChooser fileChooser = new JFileChooser();
        //afiseaza la centru ferestrei mama
       int rezult= fileChooser.showOpenDialog(EmployeeCatalog.this);

        if(rezult==JFileChooser.APPROVE_OPTION){
            File file = fileChooser.getSelectedFile();
            EmployeeDao employeeDao = new EmployeeDaoImpl();
            try {
                List<Employee> list =
                        employeeDao.read(file.getAbsolutePath());
                EmployeeTableModel model = (EmployeeTableModel) table.getModel();
                model.setData(list);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this,
                        "Invalid path. Try again.");
            }
        }
    }


}
