package md.convertit.lectia33.gui.model;

import md.convertit.lectia32.model.Employee;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 30.01.2017.
 */
public class EmployeeTableModel extends AbstractTableModel {

    private String[] columnNames = {"Name", "Salary", "Department"};
    private List<Employee> data = new ArrayList<>();

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex){
            case 0:
                return data.get(rowIndex).getName();
            case 1:
                return data.get(rowIndex).getSalary();
            case 2:
                return data.get(rowIndex).getDepartment();
            default:
                return "unknown";
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public List<Employee> getData() {
        return data;
    }

    public void setData(List<Employee> data) {
        this.data = data;
        fireTableDataChanged();
    }

    public void addEmployee(Employee employee) {
        data.add(employee);
        fireTableDataChanged();
    }

    /**
     * Stergem Employees din lista 'data' dupa index. Indexul coincide cu
     * randul din tabela
     * @param selectedRows   - randurile selectate din tabela. Ordinar 1,3,4
     */
    public void deleteEmployees(int[] selectedRows) {
        for (int i = 0; i < selectedRows.length; i++) {
            System.out.println(selectedRows[i]);
            data.remove(selectedRows[i]-i);
        }
        fireTableDataChanged();
    }
}
