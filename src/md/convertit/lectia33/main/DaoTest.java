package md.convertit.lectia33.main;

import md.convertit.lectia32.model.Employee;
import md.convertit.lectia33.dao.EmployeeDao;
import md.convertit.lectia33.dao.impl.EmployeeDaoImpl;

import java.io.IOException;
import java.util.List;

/**
 * Created by Utilizator on 27.01.2017.
 */
public class DaoTest {

    public static void main(String[] args) {
        EmployeeDao employeeDao = new EmployeeDaoImpl();
        try {
            List<Employee> myList = employeeDao.read("employees.csv");
            System.out.println(myList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
