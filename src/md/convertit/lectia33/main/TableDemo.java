package md.convertit.lectia33.main;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Utilizator on 27.01.2017.
 */
public class TableDemo extends JFrame {

    public TableDemo() throws HeadlessException {
        setTitle("Table");
        setSize(500, 300);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        createDemoTable();
        setVisible(true);
    }

    public static void main(String[] args) {
        TableDemo demo = new TableDemo();
    }

    // Motoda de creare a tabelei
    private void createDemoTable() {
        String[] columnNames = {"Nume", "Prenume", "Virsta"};
        String[][] data = {
                {"Dodon", "Igor", "45"},
                {"Voronin", "Vladimir", "74"},
                {"Grecianii", "Zinaida", "98"}
        };

//        JTable table = new JTable(7, 5); // tablela cu 7 rindusi si 5 coloane
        JTable table = new JTable(data, columnNames); // tabela cu data implicite

        // tabela sa se intinda pe toata suprafata componentei parinte
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        getContentPane().add(scrollPane);
    }
}
