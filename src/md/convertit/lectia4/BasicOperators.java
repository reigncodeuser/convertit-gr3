package md.convertit.lectia4;

/**
 * Created by Utilizator on 07.11.2016.
 */
public class BasicOperators {

    public static void main(String[] args) {
        int x = 10;
        int y = 7;
        //adunarea
        int rezultatAdunare = x + y;
        System.out.println("rezultat adunare: " + rezultatAdunare);
        //scaderea
        int rezultatScadere = x - y;
        System.out.println("rezultat scadere: " + rezultatScadere);
        //inmultirea
        int rezultatInmultire = x * y;
        System.out.println("rezultat inmultire: " + rezultatInmultire);
        int rezultatImpartire = x / y;
        System.out.println("rezultat impartire: " + rezultatImpartire);

        double rezultatImpartireDouble = x / y;
        System.out.println("rezultatImpartireDouble: " + rezultatImpartireDouble);

        float d1 = 10;
        int d2 = 7;
        System.out.println(d1 / d2);

        //% (Modulus)
        System.out.println(10 % 5); // 0
        System.out.println("rest dintr 10 si 4:" + 10 % 4); // 2

        //incrementarea
        int numarInitial = 1;
        numarInitial++; // efect: numarInitial = numarInitial +1;
        System.out.println("incrementarea " + numarInitial);

    }
}
