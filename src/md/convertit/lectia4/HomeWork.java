package md.convertit.lectia4;

/**
 * Created by dench on 09.11.2016.
 */
public class HomeWork {

    /**
     * Cream un program in care realizam urmatoarele
     *
     *
     * 1. declaram 3 variabile de tip double, numele lor sa fie : pretPaine, pretLapte, pretOrez
     * 2. atribuim valori pentru variabilele de mai sus : 6.65, 11.40, 42.00
     * 3. afisam la consola pretul pentru fiecare produs in formatul de mai jos :
     *   Produsul : Paine are pretul {pretPaine}
     *
     *
     *    4. cream un array de tip string, numele : theWeek cu urmatoarele valori initiale :
     *      Luni, Marti, Miercuri, Joi, Vineri, Sambata, Duminica
     *
     afisam  la consola elementul de pe index 0
     afisam  la consola elementul de pe index 4
     afisam la consola marimea tabloului
     afisam mesaje la consola pentru fiecare index din tablou, de la 0 pana la 6 , insa sa fie personalizate in dependenta de zi
     format mesaj :
     ziua {array[0]} este prima zi a saptaminii
     marti - a 2 zi
     miercuri - e zi din mijlocul saptaminii
     joi - vine weekeeeend !
     vineri - azi ii vineri
     sambata - super , azi ii weekend
     duminica - inca mai super , dorm cat vreau :Dog

     *
     */


    /**
     Realizam un program nou in alta clasa java :
     Declaram si initilizam un tablou cu marimea initiala 4
     Folosind clasa Scanner (java.util) rugam utilizatorul sa introduca valori pentru fiecare index al tabloului
     ex: Va rugam inserati o valoare pentru index 0 al array-ului
     myArray[0] = scanner.nextInt()
     la fel se procedeaza si pentru celelalte

     dupa ce utilizitorul a completat toate index-uril  afisati la consola mesaje dupa formatul:
     Pe indexul {0} al tabloului avem valoarea : {5}

     apoi atribuim valori aceluasi tablou numere aleatorii folosind clasa Random
     si afisam la consola valorile dupa formatul:
     Pe indexul {0} al tabloului avem numarul aleatoriu : {24543}
     */

}
