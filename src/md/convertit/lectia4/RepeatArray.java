package md.convertit.lectia4;

/**
 * Created by Utilizator on 07.11.2016.
 */
public class RepeatArray {
    public static void main(String[] args) {
        //declaram o variabila de tip array de char, numele letters
        //si initializam array-ul letters cu 4 valori implicite: A,a,B,b
        char[] letters = {'A','a','B','b'};
        //declaram un array de tip int cu marimea 2, numele myArray
        int[] myArray = new int[2];
        //atribuim pe index  0 valoarea : 100
        myArray[0]=100;
        //atribuim pe index  1 valoarea : 200
        myArray[1]=200;

        //declaram o variabila de tip int firstItem si atribuim valoarea
        //elementului de pe index 0 din myArray
        int firstItem =  myArray[0];
        //declaram un array de tip int, numele otherArray si atribuim
        //valorile din myArray
        int[] otherArray = myArray;
        //afisam la consola valoarea lui firstItem si valoarea primului element
        //din array-ul otherArray
        System.out.println(firstItem);
        System.out.println(otherArray[0]);
    }
}
