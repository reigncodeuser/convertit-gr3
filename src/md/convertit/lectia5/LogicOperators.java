package md.convertit.lectia5;

/**
 * Created by Utilizator on 09.11.2016.
 */
public class LogicOperators {
    public static void main(String[] args) {
        // declaram 2 variabile de tip boolean
        boolean adevarat = true;
        boolean minciuni = false;
        // operator and :
        System.out.println("true && true: " + (true && true));
        System.out.println("true && false: " + (true && false));
        System.out.println("false && false: " + (false && false));
        // |
        System.out.println("********************");
        System.out.println("true || true: " + (true || true));
        System.out.println("true || false: " + (true || false));
        System.out.println("false || true: " + (false || true));
        System.out.println("false | true: " + (false | true));
        System.out.println("false || false: " + (false || false));

        boolean resultat = (5 < 10) || (9 == 9);
        System.out.println(resultat);
        resultat = (true) && (8 < -3) || (10 == 10);
        System.out.println(resultat);

    }
}
