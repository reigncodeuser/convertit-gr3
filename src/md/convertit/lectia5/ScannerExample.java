package md.convertit.lectia5;


// date introduse de la input, numere random

import java.util.Scanner;

/**
 * Created by Utilizator on 09.11.2016.
 */
public class ScannerExample {
    public static void main(String[] args) {
        //am declarat si initilizat un obiect de tip
        //Scanner
        Scanner scanner = new Scanner(System.in);
        //stochez rezultatul metode nextInt() intr-o variabila int
        System.out.println("Introducem un numar intreg si apasam ENTER");
        int numar = scanner.nextInt();
        System.out.println("Ati introdus: " + numar);
        System.out.println("Introduce-ti numele Dvs.");
        String nume = scanner.next();
        System.out.println("Numele este: " + nume);
    }
}
