package md.convertit.lectia6;

/**
 * Created by Utilizator on 11.11.2016.
 */
public class ElseIfStatememt2 {
    public static void main(String[] args) {
        //declaram variabila de tip int, nota
        int nota;
        //o initilizam
        nota = 0;
        if ((nota > 0) && (nota < 5)) { // 1,2,3,4
            System.out.println("Sorry, ai nota negativa");
        } else if ((nota >= 5) && (nota <= 8)) {//5,6,7,8
            System.out.println("E bine");
        } else if ((nota == 9) || (nota == 10)) {
            System.out.println("Bravooo");
        } else {
            System.out.println("Numar invalid :( ");
        }
    }
}
