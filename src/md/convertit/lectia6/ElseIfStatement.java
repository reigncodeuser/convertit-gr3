package md.convertit.lectia6;

/**
 * Created by Utilizator on 11.11.2016.
 */
public class ElseIfStatement {

    public static void main(String[] args) {
        //nota : 10, 5, 3
        //10: Bravo,  ai 10
        //5: aproape de fail
        //3: fail total
        int nota = -10;

        if (nota == 10) {
            System.out.println("Bravo ai 10");
        } else if (nota == 5) {
            //block care se executa daca e true expresia din else if
            System.out.println("nota 5, aproape de fail");
        } else if (nota == 3) {
            //block care se executa daca e true expresia din else if
            System.out.println("nota 3, fail total");
        } else {
            //block care se executa cand nici una din expresiile
            //de mai sus nu sunt adevarate
            System.out.println("Nota aceasta nu o pot evalua :( ");
        }

    }
}
