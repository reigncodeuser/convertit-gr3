package md.convertit.lectia6;


public class HomeWork {

    /**
     *  Creati un program care o sa roage utilizatorul sa introduca suma de bani care o are cu el.
     *  Dupa ce a introdus suma, afisati urmatorele mesaje in depedenta de cati bani are:
     *  Daca are mai putin de 0 lei (minus) : Oho , dapai tu ai datorii mari si nu poti cumpara nimic
     *  Daca are intre 0 si 20 lei :  Cu {suma} lei poti merge la cantina sa mananci orez , orez de 2 zile
     *  Daca are intre 20 si 50 lei : Cu {suma}  lei  poti cumpara un pachet de orez .
     *  Daca are intre 50 si 200 lei : Cu {suma} lei mananci bine si inca ceva bun la desert.
     *  Daca are mai mult de 200 lei: Cu {suma} lei mananci bine, si inca raman bani pentru distractii.
     *
     *  Realizati programul si testati cu diferite valori introduse de utilizator
     */

}
