package md.convertit.lectia6;

/**
 * Created by Utilizator on 11.11.2016.
 */
public class IfStatement {
    public static void main(String[] args) {
        boolean isDay = true;
        System.out.println("Inainte de IF");
        // daca afara este zi, afisez mesajul : Buna Ziua
        if (isDay) {
            //mesajul apare numai in cazul cand isDay = true
            System.out.println("Buna ziua, suntem in if");
        }
        System.out.println("Dupa if");
    }
}
