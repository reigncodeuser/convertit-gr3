package md.convertit.lectia6;

import java.util.Scanner;

/**
 * Created by Utilizator on 11.11.2016.
 */
public class ScanerElseIfExample {
    public static void main(String[] args) {
        //declaram variabila de tip int, nota
        int nota;
        //o initilizam
        // nota = 0; o initiliazam cu o valoare introdusa de utilitator
        //print intermediul tasturii
        //mai intii initilizez un obiect de tip scanner
        Scanner scanner = new Scanner(System.in);
        // apelez metoda .nextIn() din scanner si il atribui variabile "nota"
        //mai intii afisam mesaj pentru utilizator
        System.out.println("Intruduceti nota Dvs.");
        nota = scanner.nextInt();
        if ((nota > 0) && (nota < 5)) { // 1,2,3,4
            System.out.println("Sorry, ai nota negativa");
        } else if ((nota >= 5) && (nota <= 8)) {//5,6,7,8
            System.out.println("E bine");
        } else if ((nota == 9) || (nota == 10)) {
            System.out.println("Bravooo");
        } else {
            System.out.println("Numar invalid :( ");
        }
    }
}
