package md.convertit.lectia6;

import java.util.Random;
import java.util.Scanner;

/**
 * Programul nostru la pornire afiseaza un mesaj de salut:
 * Bine ati venit la Loteria 2, va rugam introduceti numele Dvs.
 * Dupa ce utilizatorul introduce numele, programul alege un numar aleator
 * cu valori intre 0 si 10
 * Numerele castigatoare sunt:
 * 3 - Ferari
 * 5 - O vacanta la Roma
 * 7 - Un apartament
 * 9 - O vacanta la Zatoca
 * <p>
 * In dependenta de numarul aleator, daca este castigator afisam mesaj
 * dupa formatul :
 * {Nume} , felicitari, ati castigat {premiu}.
 * Daca nu a castigat nimic, afisam mesaj dupa format:
 * {Nume} , ne pare rau, mai incercati odata.
 * <p>
 * Afisam mesaj de adio
 */
public class SuperLoto2 {
    public static void main(String[] args) {
        String nume; //declaram o variabila de tip String
        int numarAles; //declaram o variabila de tip int
        Scanner scanner = new Scanner(System.in); //initializam scannerul
        Random random = new Random(); // initiliazam un obiect Random

        System.out.println("Bine ati venit la loterie, va rugam sa indtroduceti numele Dvs."); //afisam mesaj de salut
        nume = scanner.next(); //initilizam variabila nume cu valoare introdusa de la tastatura
        //alegem un numar aleator cu obiectul "random" de la 0 la 10;
        numarAles = random.nextInt(11); // va returna numere de la 0 la 10
        String mesaj; //declar o varibila de tip String pentru mesajul final
        if (numarAles == 3) {
            mesaj = nume + ", ati castigat un Ferari";
        } else if (numarAles == 5) {
            mesaj = nume + ", ati castigat  vacanta la Roma";
        } else if (numarAles == 7) {
            mesaj = nume + ", ati castigat un apartament";
        } else if (numarAles == 9) {
            mesaj = nume + ", ati castigat o vacanta la Zatoca";
        } else {
            mesaj = nume + ", ne pare rau, mai incercati odata";
        }
        System.out.println("Numarul ales esteeeeee: " + numarAles);
        //afisam la consola variabila mesaj
        System.out.println(mesaj);
        //afisam mesajul de adio
        System.out.println("Va multumim ca ati participat la Super Loto 2");
    }
}
