package md.convertit.lectia7;

/**
 * Created by Utilizator on 14.11.2016.
 */
public class ArrayIteratorExample {
    public static void main(String[] args) {
        //declram si initilizam un array de tip String
        //cu urmatoarele valori initiale:
        String[] elevi = {"Ionel", "Vasile", "Igoras", "Mihaela", "Ana", "Ala"};
        //folosim foor loop pentru a afisa elementela din tablou

        // System.out.println("Pe index " + 0 + " se afla: " + elevi[0]);
        int repetari = elevi.length;
        for (int i = 0; i < elevi.length; i++) {
            System.out.println("Pe index " + i + " se afla: " + elevi[i]);
        }
        //desenam stelute folosin foor loop

        for (int i = 0; i < 30; i++) {
            System.out.print("*");
        }

        System.out.println();
        //afisam la consola fiecare al 2lea elev, folosind for loop
        for (int i = 0; i < elevi.length; i = i + 2) {
            System.out.println("Pe index " + i + " se afla: " + elevi[i]);
        }

    }
}
