package md.convertit.lectia7;

/**
 * Created by dench on 15.11.2016.
 */
public class HomeWork {
    /**
     *  1 . Care este structura expresiei for ?
     *
     *  2. Cream un program in care cu ajutorul lui 'for' sa ne afiseze la consola  de 10 ori un mesaj dupa formatul:
     *   "La momemnt i are valoarea" + i
     *
     *   3.  Declaram si initializam cu 0 o variabila de tip int, numele 'suma'.
     *   Folosind 'for', cream 10 iteratii in care variabilei 'suma' aduname de fiecare data valoarea lui 'i' din for,
     *  Afisam la consola valoarea sumei calculate.
     *
     *  4. Declaram un array de tip int cu urmatoarele valori initiale  2,3,6,1,99
     *   a. afisam la consola, folosind 'for' mesaje dupa formatul:
     *    Pe index {i} al tabloului {numeTablou} avem valoarea {valoarea} //ex: Pe index 0 al tabloului myArray avem valoarea 2
     *
     *    b. Calculam suma tuturor elementelor din tablou, folosind 'for' si o afisam la consola
     *
     *   5. Cream un tablou de tip char cu urmatoarele valori initiale : c, C , u,U , z, Z
     *      a. afisam la consola mesaj dupa formatul:
     *      Pe index {i} al tabloului {numeTablou} este carcterul: {valoare}
     *
     *      //Optional
     *      Folosind for, pentru tabloul cu variabile char , afisam la consola valorile in int pentru elementele tabloului
     *      (ex. cum la lectie am aratat pentru A -> 65 )
     */
}
