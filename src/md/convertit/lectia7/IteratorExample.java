package md.convertit.lectia7;


/**
 * Created by Utilizator on 14.11.2016.
 */
public class IteratorExample {
    public static void main(String[] args) {
        //0
        //1
        //2
        //3
        //4
        //5
     /*   System.out.println(0);
        System.out.println(1);
        System.out.println(2);
        System.out.println(3);
        System.out.println(4);
        System.out.println(5);*/

        for (int i = 0; i <= 5; ++i) {
            System.out.println(i);
        }
        System.out.println("*******");

        for (int j = 10; j >= 0; j--) {
            System.out.println(j);
        }
    }
}
