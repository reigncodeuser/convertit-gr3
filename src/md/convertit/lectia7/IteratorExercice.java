package md.convertit.lectia7;

import java.util.Random;

/**
 * Created by Utilizator on 14.11.2016.
 */
public class IteratorExercice {
    public static void main(String[] args) {
        //declaram un tablou de tip int , cu marimea initiala 10;
        //atribuim in mod dinamic valori de la 0 la 10 folosind clasa
        //Random
        int[] numere = new int[10];
        Random random = new Random();
        //atribum fiecarui element din tablou cate o valoare aleatorie
        for (int i = 0; i < numere.length; i++) {
            numere[i] = random.nextInt(11);
        }
        //apoi afisam toate valori dupa urmatorul format:
        // Pe index[index] avem valoarea : {valoareDePeIndex}
        for (int i = 0; i < numere.length; i++) {
            System.out.println("Pe index " + i + " avem valoarea: " + numere[i]);
        }
    }
}
