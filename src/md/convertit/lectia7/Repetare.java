package md.convertit.lectia7;

/**
 * Created by Utilizator on 14.11.2016.
 */
public class Repetare {
    public static void main(String[] args) {
        //declaram o variabila de tip 'char', numele firstLetter , valoarea A
        char firstLetter = 'A';
        //declaram o variabila de tip 'char', numele secondLetter , valoarea B
        char secondLetter = 'B';

        //afisam la consola ambele variabile
        System.out.println(firstLetter);
        System.out.println(secondLetter);

        int firstNumber = firstLetter;
        System.out.println(firstNumber);
        System.out.println((int) secondLetter);
        System.out.println((int) 'b'); //aratam valoare int pentru 'b'
        //adumare a 2 variabile de tip char
        System.out.println('A' + 'a');
        //tranformam numarul 70 in char
        char ch = 70;
        System.out.println(ch);
    }
}
