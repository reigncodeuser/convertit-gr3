package md.convertit.lectia7;

/**
 * Created by Utilizator on 14.11.2016.
 */
public class Simple2IteratorExample {

    public static void main(String[] args) {
        // adunarea toturor cifrelor de la 0 la 10
        int suma = 0;
        suma = suma + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10;
        System.out.println("Suma este = " + suma);

        // for as face:
        int suma2 = 0;
        for (int i = 0; i <= 10; i++) {
            suma2 = suma2 + i;
        }
        System.out.println("Suma2 este = " + suma2);
    }
}
