package md.convertit.lectia7;

/**
 * Created by Utilizator on 14.11.2016.
 */
public class SimpleIteratoreExample {
    public static void main(String[] args) {
        // afisam de la 0 la 100 cu ajutorul lui for urmatorul mesaj:
        // 0 1 2 3 4
        for (int i = 0; i < 100; i++) {
            System.out.print(i+" ");
        }

        System.out.println();
        // scoatem fiecare a 4-a cifra pina la 100
        for (int i = 0; i < 100; i=i+4) {
            System.out.print(i+" ");
        }

        // scoatem de 10 ori mesajul "Buna ziua"
        for (int i = 0; i < 100; i++) {
            System.out.print("Buna ziua");
        }
    }
}
