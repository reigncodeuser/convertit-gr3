package lectia8;

/**
 * Created by Utilizator on 16.11.2016.
 */
public class DoWhileExample {
    public static void main(String[] args) {

        // while( <expresieBooleana>) {
        // block care se executa cand expresia booleana e adevarata  }
        int i = 0;
        System.out.println("intram in while");
        do {
            System.out.println(++i);
        } while (i > 20);

        System.out.println("am iesit din while");

    }
}
