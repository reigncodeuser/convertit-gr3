package lectia8;

/**
 * Created by Utilizator on 16.11.2016.
 */
public class ForBreakExample {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            // System.out.println(i);
            if (i == 5 || i == 6) {
                System.out.println(i);
            }
        }
        System.out.println("*******  il cautam pe 5  ****");
        for (int i = 0; i < 10; i++) {
            System.out.println("la moment i este: " + i);
            if (i == 5) {
                System.out.println("l-am gasit pe 5");
                break; //opreste iteratiile curente
            } else {
                System.out.println("Nu l-am gasit pe 5, mai incercam");
            }
        }
        System.out.println("Am iesit din for, mergem la pauza");
    }
}
