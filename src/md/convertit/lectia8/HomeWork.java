package md.convertit.lectia8;

/**
 * Created by Dennis on 17.11.2016.
 */
public class HomeWork {
    /**
     *  1.
     * Cream un program care afiseaza la consola numerele de la 0 la 50
     *
     * Modificam programul in asa fel ca sa ne arate mesaje dupa conditia urmatoare
     *  Daca numarul este 10, 20, 30 , sau 40, mesaj dupa formatul:
     *   Numarul {numar} este un numar rodund
     *   , pentru celelalt
     *   Numarul {numar} este un numar obisnuit
     *
     *   OPTIONAL : pentru numele care se se impart la 5 fara rest mesaj dupa formatul:
     *     Numarul {numar} este devizibil la 5
     */


    /**
     *  2. Declaram un tablou de tip int si initializam cu urmatoarele valori initiale
     *   23,44,-23,-43,300, 85
     *
     *    a)Iteram prin elementele tabloului si afisam toatele valorile din tablou
     *
     *    b) mai iteram odata prin toate si afisam numai numerele pozitive
     *
     *    c) mai iteram odata si afisam doar numerele negative
     *
     *    d) mai interam odata si afisam mesaje diferite in depedenta de negativ/pozitiv
     *     ex: Nr {x} de pe index {y} al tabloului este negativ/pozitiv
     *
     *     (nota: trebuie sa avem 4 for-uri)
     *
     *     Optional: La punctul d incercam sa folosim 'while'
     *
     *
     */


    /**
     * 3. Declaram si un array de tip String cu marimea initiala 3
     *  adaugam pe index 0 valoarea : Mama
     *  adaugam pe index 1 valoarea : Tata
     *  adaugam pe index 2 valoarea : Sora
     *  adaugam pe index 3 valoarea : Frate
     *
     *   Iteram prin array si aratam toate elementele lui.
     *
     *   Mai declaram un array de tip String cu valorile initiale : Bunel, Bunica, Unchi, Matusa
     *  Iteram prin array si aratam toate elementele lui.
     *  atribuim elementului de pe index 0 din primul array, valoare elementului de pe index 0 din al 2lea array
     *  Iteram din nou prin toate elementele primului array si afisam elementele lui.
     */
}
