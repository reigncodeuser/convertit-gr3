package lectia8;

/**
 * Created by Utilizator on 16.11.2016.
 */
public class WhileBreakExample {

    public static void main(String[] args) {

        int i = 0;

        while (true) {
            System.out.println("i este: " + i++);
            if (i == 5) {
                System.out.println("am ajuns la 5 si oprim iteratiile");
                break;
            }
        }
    }
}
