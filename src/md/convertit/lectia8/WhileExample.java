package lectia8;

/**
 * Created by Utilizator on 16.11.2016.
 */
public class WhileExample {
    public static void main(String[] args) {

        // while( <expresieBooleana>) {
        // block care se executa cand expresia booleana e adevarata  }
        int i = 0;
        System.out.println("intram in while");
        while (i < 20) {
            System.out.println(i++);
        }
        System.out.println("am iesit din while");

    }
}
