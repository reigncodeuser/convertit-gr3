package md.convertit.lectia9;

/**
 * Created by dench on 19.11.2016.
 */
public class HomeWork {

    /** **
     *  1- Ce este declararea unei variabile ?
     *  2- Ce este initializarea unei variabile ?
     *  3- poate fi o variabila declarata si initiliazata in acelasi timp ?
     *  4-  stabiliti tipul de date pentru urmatoarele variabile :
     *    a)  -15
     *    b)  24D
     *    c) 24L
     *    d) 4.54
     *    e) 'c'
     *    f) 'C'
     *    h) '5'
     *    i) "5"
     *    j) true
     *
     *  5- care este structura expresiei if ?
     *  6- care este structura expresiei else if ?
     *  7- care este structura expresiei for ? (scrienti un exemplu)
     *  8- folosind for si if , iteram de la 0 la 10 si afisam la consola numai numelere 4,6,7 si 10
     *
     *   9. Switch Case
     *
     *   Declaram o variabila de tip String cu numele : message;
     *   Creati un program, care sa afisez urmatoarele mesaje in depedenta de valoarea variabilei: int monthInYear
     *    In cazul care monthInYear este : 12,1,2  initilizam variabila 'message' cu valoarea "Este iarna"
     *    In cazul care monthInYear este : 3,4,5  initilizam variabila 'message' cu valoarea "A venit primavara"
     *    In cazul care monthInYear este : 6,7,8  initilizam variabila 'message' cu valoarea "E vara afara"
     *    In cazul care monthInYear este : 9,10,11  initilizam variabila 'message' cu valoarea "E toamna in calendar"
     *    In celelalte cazuri : numarul lunii este invalid.
     *
     *    Afisam la consola valoarea variabile message.
     *
     *    Executam programul de mai multe ori si incercam diverite valori pentru variabila 'monthInYear'
     *
     *
     *    10. String
     *    Intr-un program nou declaram 3 obiecte de tip String , s1, s2, s3
     *   initializam s1 si s2 cu valorile :  pilot si pilotare
     *   initlizam s3 cu valoare "pilotare" folosind cuvantul cheie 'new'
     *    a) afisam la consola lungimea tutoror stringurilor de mai sus
     *    b) concatenam s1 cu s2 si afisam la consola stringul concatenat
     *    c) verificam daca s2 contine pe s1 (true sau false)
     *    d) verificam daca s2 si s3 sunt egale
     *    e) verificam daca s3 si s3 au referintele egale
     *    f) afisam caracterul al 3-lea din s1 este egal cu caracterul al 4-lea din s3
     *    g) care este 'codePoing' pe index 0 al s1, si codePoint pe ultimul index al s2
     *    f) transformam toate stringurile cu majuscule si le afisam
     *    g) declaram o variabila de tip String , nume, si o initilizam cu numele nostru
     *    f) afisam la ecran cate litere are numele nostru
     *    g) afisam la ecran numele nostru scris cu majuscule
     *    h) OPTIONAL , modificam programul in asa fel ca sa scrim numele nostru de la tastatura.
     *
     */

}
