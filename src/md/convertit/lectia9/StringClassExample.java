package md.convertit.lectia9;


/**
 * Created by Utilizator on 18.11.2016.
 */
public class StringClassExample {
    public static void main(String[] args) {
        // int primitivValue = 10;
        String txt = "mama";
        System.out.println("string-ul nostru este: " + txt);
        //afisam caracter de pe index 2 al string-ului txt ( .charAt(int index))
        char chartAt2 = txt.charAt(2);
        System.out.println("char pe index 2 este :" + chartAt2);
        int codePointAt1 = txt.codePointAt(1);
        System.out.println("code point pe index 1: " + codePointAt1);
        // testam daca txt se termina cu 'ma'
        boolean endsWithMa = txt.endsWith("ma");
        System.out.println("Se termina cu ma ?: " + endsWithMa);
        //testam daca se termin cu 'ta'
        boolean endsWithTa = txt.endsWith("ta");
        System.out.println("Se termina cu ta ?: " + endsWithTa);
        //testam daca se termina cu 'Ma'
        boolean endsWithMA = txt.endsWith("Ma");
        System.out.println("Se termina cu Ma ?: " + endsWithMA);

        //le facem majuscule:
        String upperString = txt.toUpperCase();
        System.out.println("dupa toUpperCase: " + upperString);
        txt = txt.toUpperCase();
        System.out.println("aceasi referinta: " + txt);
    }
}
