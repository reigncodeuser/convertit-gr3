package md.convertit.lectia9;

/**
 * Created by Utilizator on 18.11.2016.
 */
public class StringEqualsExample {
    public static void main(String[] args) {
        String s1 = "mama";
        String s2 = new String("mama");

        boolean suntEgale = s1.equals(s2);
        System.out.println("sunt egale: " + suntEgale);
        //verifica daca au referintele egale:
        boolean referinteEgale = (s1 == s2);
        System.out.println("Referinte egale: " + referinteEgale);

        String s3 = "tata";
        String s4 = "tata";
        //refolosim variabila referinteEgale
        referinteEgale = (s3==s4);
        System.out.println("tata-tata ref egale: " + referinteEgale);
    }
}
