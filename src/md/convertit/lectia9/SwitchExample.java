package md.convertit.lectia9;

/**
 * Created by Utilizator on 18.11.2016.
 */
public class SwitchExample {
    public static void main(String[] args) {
        //declaram o variabila de tip int, numele nota
        int nota = 5;
        System.out.println("Avem nota: " + nota);
        //afisam urmatoarele mesaje:
        //daca nota este 5 : Abia ai trecut
        //daca nota este6,7 sau 8 : normal, nota medie
        //daca nota este 9 sau 10 : Bravo, you are the best
        if (nota == 5) {
            System.out.println("Abia ai trecut");
        } else if (nota == 6 || nota == 7 || nota == 8) {
            System.out.println("normal, nota medie");
        } else if (nota == 9 || nota == 10) {
            System.out.println("Bravo, you are the best");
        }
        System.out.println("*********");

        switch (nota) {
            case 5:
                System.out.println("Abia ai trecut");
                break;
            case 6:
            case 7:
            case 8:
                System.out.println("normal, nota medie");
                break;
            case 9:
            case 10:
                System.out.println("Bravo, you are the best");
                break;
            default:
                System.out.println("Nota e necunoscuta pentru softul nostru");
        }

    }
}
